#ifndef CSPEEDOMETER_H
#define CSPEEDOMETER_H

#include "ARSceneControl.h"

class CSpeedometer : public  CARSceneControl
{
    Q_OBJECT
public:
    CSpeedometer(const std::string& name, std::shared_ptr<AugmentedSceneProxy<>> objRef);
    virtual ~CSpeedometer() override {}

    void initialize(const std::vector<AugmentedSceneTypes::PrimitiveInfo>& primitives_info);

    Q_PROPERTY(int curSpeed READ getSpeed WRITE setSpeed NOTIFY curSpeedChanged)
    Q_PROPERTY(bool showSpeedometr READ isSpeedometrVisible WRITE setSpeedometerVisible)
    Q_INVOKABLE void setSpeed(int value);

signals:
    void curSpeedChanged();

public slots:

public:


protected:
    CSpeedometer() : CARSceneControl("Invalid", nullptr) {}

    bool isSpeedometrVisible() const { return m_speedometr; }
    void setSpeedometerVisible(bool val);
    int getSpeed() const { return m_speed; }
    void updateSpeedValue(bool valid, int value);

protected:
    int m_speed = 10;
    bool m_speedometr = true; // false - Speedometer is invisible
};

#endif // CSPEEDOMETER_H
