//#include "JSItem.hpp"
#include "JSModules.h"

#include <QFileInfo>
#include <QFile>
#include <QJSEngine>
#include <QJSValue>

#include <QDebug>

CJSItem::CJSItem(const QString& name, EModuleType type, QString& dscText, CJSItem* parentItem)
 :  m_itemType(type),module_name(name), m_itemDescription(dscText), m_parent(parentItem)
{

}

CJSItem::~CJSItem()
{
   //qDebug() << "~CJSItem";
   qDeleteAll(m_subItems);
}

void CJSItem::intialize(const QString& path)
{
  m_path = path;
  m_jsBody="";

  QString fileName = path+"\\"+module_name+".qs";
  qDebug() << "open file=" << fileName;
  if (!isFileExists(fileName) /*|| m_jsBody.length() > 0*/)
  {
     emit notifyJSBodyChanged();
     return;
  }

  QFile scriptFile(fileName);
  if (!scriptFile.open(QIODevice::ReadOnly))
      return;

  QTextStream stream(&scriptFile);
  m_jsBody = stream.readAll();
  scriptFile.close();

  emit notifyJSBodyChanged();
}

bool CJSItem::isFileExists(const QString& path)
{
   QFileInfo check_file(path);

   return check_file.exists() && check_file.isFile() ? true : false;
}

void CJSItem::save()
{
    if (!isJSHandler())
    {// save childs
      foreach(CJSItem* item, m_subItems)
      {
        item->save();
      }
      return;
    }

    QString fileName = m_path+"\\"+module_name+".qs";

    QFile file(fileName);
    file.open(QIODevice::WriteOnly);
    file.write(m_jsBody.toUtf8());
    file.close();
}

void CJSItem::load(const QString& path)
{
    if (!isJSHandler())
    {// save childs
      foreach(CJSItem* item, m_subItems)
      {
        item->load(path);
      }
      return;
    }

    intialize(path);
}

QSharedPointer<QJSEngine> CJSItem::getJSEngine()
{
  return (m_TreeModel) ? m_TreeModel->getJSEngine() : nullptr;
}

void CJSItem::execJS()
{
    if (m_jsBody.isEmpty())
    {
        return;
    }

    QJSValue engValue =  getJSEngine()->evaluate(m_jsBody);
    if (engValue.isError())
    {
        qDebug()
                << "Uncaught exception at line"
                << engValue.property("lineNumber").toInt()
                << ":" << engValue.toString();        
    }else
    {
      QJSValue retValue = engValue.call();
      qDebug() << "return:" << retValue.toString();
    }
}

void CJSItem::valudateJS()
{
  m_jsValidationRes = "JavaScript is OK";
  QJSValue engValue =  getJSEngine()->evaluate(m_jsBody);
  if (engValue.isError())
  {
      m_jsValidationRes = "Uncaught exception at line" + engValue.property("lineNumber").toString()
              + ":" + engValue.toString();
  }

  emit notifyJSValidated();
}
