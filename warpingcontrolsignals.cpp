#include "warpingcontrolsignals.h"

#include <QString>
#include <QDebug>

#include "glm/glm.hpp"

const uint8_t matrixHeight = 21;
const uint8_t matrixWidth = 21;

struct WarpMatrixData
{
    std::string name;
    WarpingControlTypes::MatrixId id;
    v1::Ipc::CommonTypes::Radians angle;
    WarpingControlTypes::Matrix matrix;
};


CWarpingControlSignals::CWarpingControlSignals(QObject *parent)
    : QObject(parent)
{

}

CWarpingControlSignals::CWarpingControlSignals(std::shared_ptr<WarpingControlProxy<>> objRef)
    : m_proxy(objRef)
{

}

bool CWarpingControlSignals::isAvailable() const
{
    // qDebug() << "WarpingControl Proxy: " << m_proxy->isAvailable();
    return (m_proxy && m_proxy->isAvailable());
}

void CWarpingControlSignals::setupWarpingMatrices(const QString& fileLow, const QString& fileMid, const QString& fileHigh)
{
    std::vector<WarpMatrixData> warping_matrices = {
        WarpMatrixData{"LOW", 0, glm::radians(-4.0), readWarpingMatrix(matrixWidth, matrixHeight,fileLow.toStdString())},
        WarpMatrixData{"MID", 0,  glm::radians(0.0), readWarpingMatrix(matrixWidth, matrixHeight, fileMid.toStdString())},
        WarpMatrixData{"HGH", 0,  glm::radians(4.0), readWarpingMatrix(matrixWidth, matrixHeight, fileHigh.toStdString())}
    };

    qDebug() << "Setup warping matrices. num=" << warping_matrices.size();

    CommonAPI::CallStatus call_status;
    for (auto& mat : warping_matrices)
    {
        // check matrix
        if (mat.matrix.getData().size() == 0)
        {
          qCritical() << "Matrix data is empty";
          return;
        }

       // [OS] TODO: Update API
        //m_proxy->addMatrix(call_status, mat.id);
        //m_proxy->updateMatrix(mat.id, mat.angle, mat.matrix, call_status);

        qDebug() << "Added warping matrix '" << QString(mat.name.c_str()) << "'. id=" << mat.id
                  << " angle=" << mat.angle << "rad";
    }
}

WarpingControlTypes::Matrix CWarpingControlSignals::readWarpingMatrix(uint32_t width, uint32_t height, const std::string& filename)
{
    const double c_factor = 8.0;
    const std::size_t c_buffer_len = 1769;

    std::ifstream file(filename, std::ios::binary);
    if (!file)
    {
        qCritical() << "Couldn't open warping matrix file: " << QString(filename.c_str());
        return WarpingControlTypes::Matrix();
    }

    std::vector<char> buffer(
        (std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
    if (buffer.size() != c_buffer_len)
    {
        qCritical() << "Wrong of warping matrix. size=" << buffer.size();
        return WarpingControlTypes::Matrix();
    }

    std::vector<v0::Ipc::MathTypes::Vector2> matrix_data(width * height);

    const int16_t* current_val = reinterpret_cast<const int16_t*>(&buffer.front());
    double x, y;
    for (std::size_t i = 0; i < matrix_data.size(); i++)
    {
        x = (*current_val / c_factor);
        current_val++;
        y = (*current_val / c_factor);
        current_val++;

        matrix_data[i] = v0::Ipc::MathTypes::Vector2(x, y);
    }

   return WarpingControlTypes::Matrix(width, height, matrix_data);
}

void CWarpingControlSignals::updateMirrorPosition(double angle)
{
 //   m_mirror_angle= glm::radians(4 * std::sin(angle));
    m_mirror_angle= glm::radians(angle);

    qInfo() << "Update mirror position. angle=" << m_mirror_angle << "rad";

    CommonAPI::CallStatus call_status;
    // [OS] TODO Update API
    // m_proxy->updateMirrorPosition(m_mirror_angle, call_status);
}
