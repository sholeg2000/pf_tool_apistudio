#include "augmentedscenearadapter.h"

#include "treginterfaces.h"

CAugmentedSceneARAdapter::CAugmentedSceneARAdapter()
    : TProxyEntry<AugmentedSceneProxy<>>("", "")
{

}


CAugmentedSceneARAdapter::CAugmentedSceneARAdapter(std::string domain, std::string instance)
    : TProxyEntry<AugmentedSceneProxy<>>(domain, instance)
{

}

void CAugmentedSceneARAdapter::bindQmlContext(QQmlContext* ctx)
{
    m_AugmentedSceneARSignals = QSharedPointer<CAugmentedSceneARSignals>(new CAugmentedSceneARSignals(this->m_proxy));
    if (ctx != nullptr)
    {
        m_AugmentedSceneARSignals->registerQMLInterface(ctx);

      /*  ctx->setContextProperty("AugmentedARScene", &(*m_AugmentedSceneARSignals));
        ctx->setContextProperty("CameraPosition", &(m_AugmentedSceneARSignals->getCameraPosition()));
        ctx->setContextProperty("CameraRotation", &(m_AugmentedSceneARSignals->getCameraRotation()));*/
    }
}

void CAugmentedSceneARAdapter::regJSInterface(QSharedPointer<QJSEngine>& jsEngine)
{
    if (m_AugmentedSceneARSignals != nullptr)
    {
        m_AugmentedSceneARSignals->registerJSInterface(jsEngine);
    }

}
