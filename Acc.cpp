#include "Acc.h"

CAcc::CAcc(const std::string& name, std::shared_ptr<AugmentedSceneProxy<>> objRef)
    : CARSceneControl(name, objRef)
{

}

void CAcc::initialize(const std::vector<AugmentedSceneTypes::PrimitiveInfo>& primitives_info)
{
   CARSceneControl::initialize(primitives_info);

   AugmentedSceneTypes::Variant valVector(AugmentedSceneTypes::EVariantType::Vector3,  v0::Ipc::MathTypes::Vector3(30., 0., 0.));
    setPropertyValue("position", valVector);

    AugmentedSceneTypes::Variant valQuaternion( AugmentedSceneTypes::EVariantType::Quaternion,  v0::Ipc::MathTypes::Quaternion(1.0, 0., 0., 0.));
    setPropertyValue("rotation", valQuaternion);

    AugmentedSceneTypes::Variant valReal( AugmentedSceneTypes::EVariantType::Real,  1.5);
    setPropertyValue("width", valReal);

    AugmentedSceneTypes::Variant valText(AugmentedSceneTypes::EVariantType::Text, std::string("Warning"));
    setPropertyValue("state", valText);

    AugmentedSceneTypes::Variant valBool(AugmentedSceneTypes::EVariantType::Bool, m_Acc_Enabled);
    setPropertyValue("enabled", valBool);

    sendProperties();
}

void CAcc::showWarning(bool state)
{
    std::string strState =  state ? "Warning" : "Normal";
    AugmentedSceneTypes::Variant valText(AugmentedSceneTypes::EVariantType::Text, strState);
    setPropertyValue("state", valText);

    sendProperties();
}

void CAcc::enableACC(bool val)
{
    AugmentedSceneTypes::Variant valBool(AugmentedSceneTypes::EVariantType::Bool, val);
    setPropertyValue("enabled", valBool);

    sendProperties();
}
