#ifndef TREGINTERFACES_H
#define TREGINTERFACES_H

#include "tproxyentry.h"
#include "warpingcontroladapter.h"
#include "augmentedsceneadapter.h"
#include "augmentedscenearadapter.h"

#include <QDebug>

static const char* instWarpingControl = "WarpingControl";
static const char* instAugmScene = "AugmentedScene";
static const char* instAugmScene2 = "AugmentedScene-2";

class CCommInterfaces
{
 public:
    CCommInterfaces() {}

    void InitializeComAPI(QQmlContext* ctx, QSharedPointer<QJSEngine> jsEngine)
    {
       for (auto i =0; i < m_countIF; ++i)
       {
           if (rendererInterfaces[i].proxyObj->build() == false)
           {
              qDebug() << "ERROR: Proxy not created: " << rendererInterfaces[i].proxyObj->getName();
           }
           rendererInterfaces[i].proxyObj->bindQmlContext(ctx);
           rendererInterfaces[i].proxyObj->regJSInterface(jsEngine);
       }
    }

    void InitializeJSEngine(QSharedPointer<QJSEngine> jsEngine)
    {
        for (auto i =0; i < m_countIF; ++i)
        {
           rendererInterfaces[i].proxyObj->regJSInterface(jsEngine);
        }
    }

    std::shared_ptr<CBaseEntry> getProxyByName(const char* name)
    {
        for (auto i =0; i < m_countIF; ++i)
        {
            if (rendererInterfaces[i].proxyObj->getName() == name)
            {
               return rendererInterfaces[i].proxyObj;
            }
        }
    }

 protected:

    static const uint8_t m_countIF = 3;
    struct
    {
        std::shared_ptr<CBaseEntry> proxyObj;
    } rendererInterfaces[m_countIF] =
    {
        {std::shared_ptr<CBaseEntry>(new CWarpingControlAdapter("local", instWarpingControl))},
        {std::shared_ptr<CBaseEntry>(new CAugmentedSceneAdapter("local", instAugmScene))},
        {std::shared_ptr<CBaseEntry>(new CAugmentedSceneARAdapter("local", instAugmScene2))}
    };
};
#endif // TREGINTERFACES_H
