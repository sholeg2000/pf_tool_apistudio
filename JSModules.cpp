#include "JSModules.h"

#include <QtDebug>
#include "JSItem.hpp"

#include <QtQml>

CJSModules::CJSModules()
: QAbstractItemModel(nullptr)
{
}


CJSModules::~CJSModules()
{
   qDebug() << "~CJSModules";
}

CJSModules::CJSModules(QSharedPointer<QJSEngine> jsEngine)
: QAbstractItemModel(nullptr), m_jsEngine(jsEngine)
{
}

void CJSModules::createModel(QQmlContext* ctxt)
{
    QString empty;
    m_data =  QSharedPointer<CJSItem>(new CJSItem("Root", EMODULE /*module*/, empty, nullptr /* no parent*/));

    // --------- MaiLoop Module --------
    QString dscTxt = "Module contains JS handlers: InitEnvironment, HeartBeatTimer";
    CJSItem* jsUnit = new CJSItem("Main", EMODULE , dscTxt, m_data.data());
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsUnit), QQmlEngine::CppOwnership);

    /* 'InitEnvironment' */
    dscTxt = "InitEnvironment script called once when automatic mode is started and used to make initial settings (speed, show/hide elements,etc.)";
    CJSItem* jsEventHandler = new CJSItem("InitEnvironment", EJSHANDLER /* JSHandler */, dscTxt);
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsEventHandler), QQmlEngine::CppOwnership);
    jsEventHandler->setTreeModel(this);

    ctxt->setContextProperty("InitEnvironment", jsEventHandler);
    jsUnit->appendChild(jsEventHandler);

    /* 'HeartBeatTimer' called by Timer from QML*/
    dscTxt = "TODO";
    jsEventHandler = new CJSItem("HeartBeatTimer", EJSHANDLER /* JSHandler */,dscTxt);
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsEventHandler), QQmlEngine::CppOwnership);
    jsEventHandler->setTreeModel(this);

    ctxt->setContextProperty("HeartBeatTimer", jsEventHandler);
    jsUnit->appendChild(jsEventHandler);

    m_data->appendChild(jsUnit);

    // --------- Speedometer --------
    jsUnit = new CJSItem("Speedometer", EMODULE /* module */, dscTxt, m_data.data());
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsUnit), QQmlEngine::CppOwnership);

    dscTxt = "Use this property to read or change speed.";
    CJSItem* jsAttribute = new CJSItem("int curSpeed [read/write]", EATTRIBUTE /* Attribute */, dscTxt);
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsAttribute), QQmlEngine::CppOwnership);
    jsUnit->appendChild(jsAttribute);

    dscTxt = "Assign 'false' value to hide Speedometer AR primitive on the output view.";
    jsAttribute = new CJSItem("bool showSpeedometr [read/write]", EATTRIBUTE /* Attribute */, dscTxt);
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsAttribute), QQmlEngine::CppOwnership);
    jsUnit->appendChild(jsAttribute);

    m_data->appendChild(jsUnit);

    // ---- FAS Cluster -------
    jsUnit = new CJSItem("FASCluster", EMODULE /* module */, dscTxt, m_data.data());
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsUnit), QQmlEngine::CppOwnership);


    jsAttribute = new CJSItem("bool enableACC [read/write]", EATTRIBUTE /* Attribute */, dscTxt);
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsAttribute), QQmlEngine::CppOwnership);
    jsUnit->appendChild(jsAttribute);

    jsAttribute = new CJSItem("bool setACC_ISO_Img [read/write]", EATTRIBUTE /* Attribute */, dscTxt);
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsAttribute), QQmlEngine::CppOwnership);
    jsUnit->appendChild(jsAttribute);

    //Speed Limit
    jsAttribute = new CJSItem("bool enableSpeedLimit [read/write]", EATTRIBUTE /* Attribute */, dscTxt);
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsAttribute), QQmlEngine::CppOwnership);
    jsUnit->appendChild(jsAttribute);

    CJSItem* jsMethod = new CJSItem("updateFasClusterACC(bool)", EMETHOD /* EMETHOD */, dscTxt);
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsMethod), QQmlEngine::CppOwnership);
    jsUnit->appendChild(jsMethod);

    m_data->appendChild(jsUnit);

    // -------- LDW -----
    jsUnit = new CJSItem("LDW", EMODULE /* module */, dscTxt, m_data.data());
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsUnit), QQmlEngine::CppOwnership);

    jsAttribute = new CJSItem("void updateLDW(bool left_state, bool right_state)", EMETHOD /* Method */, dscTxt);
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsAttribute), QQmlEngine::CppOwnership);
    jsUnit->appendChild(jsAttribute);

    m_data->appendChild(jsUnit);
}

QSharedPointer<QJSEngine> CJSModules::getJSEngine()
{
   return m_jsEngine;
}

void CJSModules::save()
{
    m_data->save();    
}

void CJSModules::loadData(CProjectData* prj)
{
   m_data->load(prj->getPath()+"//"+prj->getProjectName());
}

QHash<int, QByteArray> CJSModules::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ModuleName] = "moduleName";
    return roles;
}

QVariant CJSModules::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

   CJSItem* item = static_cast<CJSItem*>(index.internalPointer());
   return item->getName();
}

int CJSModules::rowCount(const QModelIndex &parent) const
{
   if (parent.column() > 0)
      return 0;

    CJSItem* parentItem = (!parent.isValid()) ? m_data.data() : static_cast<CJSItem*>(parent.internalPointer());

   return parentItem->getChildsCount();
}

int CJSModules::columnCount(const QModelIndex &parent) const
{
    return 1;
}

QModelIndex CJSModules::index(int row, int column,  const QModelIndex &parent) const
{
   qDebug() << "CJSModules::index row=" << row << "Column=" << column << "QModelIndex=" << parent;

   if (row == -1 && column == -1)
       return createIndex(row, column);

   if (!hasIndex(row, column, parent))
       return QModelIndex();

   CJSItem* childItem = nullptr;
   if (!parent.isValid())
       childItem = m_data->child(row);
   else
       childItem = static_cast<CJSItem*>(parent.internalPointer())->child(row);

   qDebug() << "CJSModules::index childItem " << childItem;
   return (childItem) ? createIndex(row, column, childItem) : QModelIndex();
}

QModelIndex CJSModules::parent(const QModelIndex &index) const
{
   qDebug() << "CJSModules::parent" << index;

   if (!index.isValid())
      return QModelIndex();

   CJSItem* childItem = static_cast<CJSItem*>(index.internalPointer());
   CJSItem* parentItem = childItem->getParent();

   if (parentItem ==nullptr /* m_data.data()*/)
     return QModelIndex();

   return createIndex(parentItem->rows(), 0, parentItem);
}

CJSItem* CJSModules::selectItem(const QModelIndex &index, int indx,  CProjectData* prj)
{
   qDebug() << "CJSModules::selectItem" << index <<  "indx=" << indx << "CProjectData=" << prj;
   if (!index.isValid())
       return nullptr;


   CJSItem* item = static_cast<CJSItem*>(index.internalPointer());
  /* qDebug() << "isJSHandler="<< item->isJSHandler() << "Name=" << item->getName() << "ProjecName=" << prj->getProjectName()
            << "Path=" << prj->getPath();
  */

  /*// Reload JScript from file
   if (item->isJSHandler())
   {
     item->intialize(prj->getPath()+"//"+prj->getProjectName());
   }*/

   return item;
}
