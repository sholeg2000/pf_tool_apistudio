#include "ARSceneControl.h"
#include "QDebug"
#include "AugmentedSceneCommon.hpp"

QString Variant2String(const AugmentedSceneTypes::Variant& value)
{
   QString retVal = "<Undefined - >";
   switch(value.getType())
   {
      case AugmentedSceneTypes::EVariantType::Bool :
                         retVal = (value.getValue().get<bool>() ? "true" : "false"); break;
      case AugmentedSceneTypes::EVariantType::UnsignedInteger32 :
                         retVal = QString().number(value.getValue().get<uint32_t>()); break;
      case AugmentedSceneTypes::EVariantType::Integer32 :
                        retVal = QString().number(value.getValue().get<int32_t>()); break;                        
      case AugmentedSceneTypes::EVariantType::Real:
                         retVal = QString().number(value.getValue().get<double>()); break;
      case AugmentedSceneTypes::EVariantType::Text :
                             retVal = QString(value.getValue().get<std::string>().c_str()); break;       
      case AugmentedSceneTypes::EVariantType::Vector3 :
              {
                      v0::Ipc::MathTypes::Vector3 v3 = value.getValue().get<v0::Ipc::MathTypes::Vector3>();
                      retVal =  QString().number(v3.getX()) + "," + QString().number(v3.getY()) + "," + QString().number(v3.getZ());
              }
              break;
     case AugmentedSceneTypes::EVariantType::Polyline3 :
              {
                  retVal = "";
                   std::vector<v0::Ipc::MathTypes::Vector3>  polyLine = value.getValue().get<std::vector<v0::Ipc::MathTypes::Vector3>>();
                   for (auto i : polyLine)
                   {
                       if (retVal.isEmpty() == false)
                           retVal = retVal + ",";
                       retVal = retVal + "{" + QString().number(i.getX()) + "," + QString().number(i.getY()) + "," + QString().number(i.getZ()) + "}";

                   }
              }
              break;
       case AugmentedSceneTypes::EVariantType::Quaternion :
              {
                    v0::Ipc::MathTypes::Quaternion v3 = value.getValue().get<v0::Ipc::MathTypes::Quaternion>();
                    retVal =   QString().number(v3.getW()) + ","+ QString().number(v3.getX()) + "," + QString().number(v3.getY())
                                     + "," + QString().number(v3.getZ());
              }
              break;
   }

   //qDebug() << "Variant2String=" << retVal;
   return retVal;
}

/*   enum Literal : int32_t {
       BoolList = 3,
       RealList = 5,
       TextList = 7,
       Vector2 = 8,
       Polyline2 = 9,
       Polyline2List = 10,
       Polyline3List = 13,
       Quaternion = 14,
       Color = 15
   };
*/

CARSceneControl::CARSceneControl(QObject *parent)
 : QObject(parent)
{

}

CARSceneControl::CARSceneControl(const std::string& name, std::shared_ptr<AugmentedSceneProxy<>> objRef)
    : QObject(nullptr), m_proxy(objRef), m_name(name)
{

}

CARSceneControl::~CARSceneControl()
{

}

void CARSceneControl::initialize(const std::vector<AugmentedSceneTypes::PrimitiveInfo>& primitives_info)
{
   m_idPrimitive = getPrimitiveId(primitives_info, m_name, m_propInfo);

   CommonAPI::CallStatus call_status;
   m_proxy->addObject(getPrimitiveID(), call_status, m_idObject);
}

void CARSceneControl::removeObject()
{
    CommonAPI::CallStatus call_status;
    m_proxy->removeObject(getObjectID(), call_status);
    m_proxy->applyChanges(0, call_status);
}

void CARSceneControl::setPropertyValue(const std::string& propName,const AugmentedSceneTypes::Variant& value)
{
     //Find property by name
     auto objProp = std::find_if( m_propInfo.begin(), m_propInfo.end(),
                 [&propName](const AugmentedSceneTypes::PropertyInfo& item) {
                     return item.getName() == propName;
                 });
      if (objProp == m_propInfo.end())
      {
        qCritical()  << "Couldn't find property=" << QString(propName.c_str());
        return;
      }

     // Check Property Type
     if (objProp->getType() != value.getType())
     {
         qCritical()  << "Invalid value type=" << value.getType();
         return;
     }

     // Set Value
     m_propValues[objProp->getId()] = value;

     emit valueChanged(QString(m_name.c_str()), QString(propName.c_str()), Variant2String(value));
}

AugmentedSceneTypes::Variant CARSceneControl::getPropertyValue(const std::string& propName)
{
     //Find property by name
     auto objProp = std::find_if( m_propInfo.begin(), m_propInfo.end(),
                 [&propName](const AugmentedSceneTypes::PropertyInfo& item) {
                     return item.getName() == propName;
                 });
      if (objProp == m_propInfo.end())
      {
        qCritical()  << "Couldn't find property=" << QString(propName.c_str());
        return AugmentedSceneTypes::Variant();
      }

    return m_propValues[objProp->getId()];
}

void CARSceneControl::sendProperties()
{
    AugmentedSceneTypes::ObjectsProperties obj_prop = {
        { m_idObject, { m_propValues } }
     };

    CommonAPI::CallStatus call_status;
    m_proxy->updateObjectsProperties(obj_prop, call_status);
    m_proxy->applyChanges(0, call_status);
}
