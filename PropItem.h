#ifndef PROPITEM_H
#define PROPITEM_H

#include <QObject>

enum EPropType
{
   EROOT = 0,
   EPRIMITIVE,
   EPRIM_HOLDER_ACTIONS,
   EPRIM_ACTION,
   EPRIM_HOLDER_CMNDS,
   EPRIM_COMMAND,
   EPRIM_HOLDER_PRP,
   EPRIM_PROPERTY
};

class CPropItem : public QObject
{
    Q_OBJECT
public:
    CPropItem(const QString& name, EPropType type, CPropItem* parentItem = nullptr);
    ~CPropItem();

    QString getName() const { return m_item_name; }
    QString getValue() const { return m_value; }
    void  setValue(const QString& val) { m_value = val; }
    CPropItem* getParent() {  return m_parent; }
    EPropType getType() const { return m_item_type; }

    int getChildsCount() const { return m_subItems.count(); }
    CPropItem* child(int row) {
        if (row < 0 || row >= m_subItems.size())
            return nullptr;
          return m_subItems.at(row);
    }

    int row() {  return  (m_parent) ? m_parent->m_subItems.indexOf(this) : 0; }
    void appendChild(CPropItem* child) { m_subItems.append(child); }

signals:

public slots:

protected:  // members
    CPropItem() {}
protected: // Attributes
    QString m_item_name;
    QString m_value; /* only for EPRIM_PROPERTY */
    CPropItem* m_parent = nullptr; /* Attention: QSharedPointer can not be used due to recursive references */
    QVector<CPropItem*> m_subItems;
    EPropType m_item_type = EROOT;
};

#endif // PROPITEM_H
