#ifndef TPROXYENTRY_H
#define TPROXYENTRY_H

#include "QString"
#include "cstdint"
#include "memory"

#include <QQmlContext>
#include <QJSEngine>

#include <QDebug>
#include <CommonAPI/CommonAPI.hpp>

template <typename Proxy>
class TProxyEntry;

class CBaseEntry
{
    public:
       CBaseEntry() {}
       virtual ~CBaseEntry() {}

       virtual bool build() = 0;

       virtual QString getName() const = 0;

       virtual void bindQmlContext(QQmlContext* ctx) = 0;

       virtual void regJSInterface(QSharedPointer<QJSEngine>& jsEngine) = 0;
};

template <template <typename...> class Proxy, typename... AttributeExtensions>
class TProxyEntry<Proxy<AttributeExtensions...>> : public CBaseEntry
{
  public:
    TProxyEntry(std::string domain, std::string instance)
     : m_domain(domain), m_instance(instance)
    {

    }

    bool build() override
    {
        if (CommonAPI::Runtime::get() == nullptr)
        {
            qDebug() << "[ERROR] CommonAPI not availabe";
            return false;
        }

        m_proxy = CommonAPI::Runtime::get()->buildProxy<Proxy>(m_domain, m_instance);
        return (m_proxy != nullptr) ? true : false;
    }

    QString getName() const override
    {
      return  QString::fromLocal8Bit(m_instance.c_str());
    }


protected:
    TProxyEntry() {}

    std::string m_domain;
    std::string m_instance;
    std::shared_ptr<Proxy<>> m_proxy;
};


#endif // TPROXYENTRY_H
