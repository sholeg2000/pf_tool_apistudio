#include "FasCluster.h"

CFasCluster::CFasCluster(const std::string& name, std::shared_ptr<AugmentedSceneProxy<>> objRef)
    : CARSceneControl(name, objRef)
{

}

void CFasCluster::initialize(const std::vector<AugmentedSceneTypes::PrimitiveInfo>& primitives_info)
{
    CARSceneControl::initialize(primitives_info);

    // ACC
    AugmentedSceneTypes::Variant valText(AugmentedSceneTypes::EVariantType::Text, std::string("Active"));
    setPropertyValue("acc.target_vehicle_state", valText);
    setPropertyValue("acc.radar_waves_state", valText);
    AugmentedSceneTypes::Variant valBool(AugmentedSceneTypes::EVariantType::Bool,m_Acc_ISO);
    setPropertyValue("acc.acc_iso_state", valBool);

    // ACA
    valText.setValue(std::string("Active"));
    setPropertyValue("aca.state", valText);

    // pACC
    valBool.setValue(false);
    setPropertyValue("pacc.pacc_offset", valBool);
    valText.setValue(std::string("NoEvent"));
    setPropertyValue("pacc.event", valText);

    AugmentedSceneTypes::Variant valUInt32(AugmentedSceneTypes::EVariantType::UnsignedInteger32,static_cast<uint32_t>(50));
    setPropertyValue("pacc.speedlimit", valUInt32);

    // GRA
    valText.setValue(std::string("Active"));
    setPropertyValue("gra.state", valText);

    // LIM  - Speed Limit
    valText.setValue(std::string("Active"));
    setPropertyValue("lim.state", valText);
    valBool.setValue(false);
    setPropertyValue("lim.isa_mode", valBool);
    valUInt32.setValue(static_cast<uint32_t>(45));
    setPropertyValue("lim.speedlimit", valUInt32);
    valBool.setValue(true);
    setPropertyValue("lim.show_speedlimit", valBool);

    // PEA
    valText.setValue(std::string("Warning"));
    setPropertyValue("pea.state", valText);
    valText.setValue(std::string("Crossing"));
    setPropertyValue("pea.event", valText);
    valUInt32.setValue(static_cast<uint32_t>(60));
    setPropertyValue("pea.speedlimit", valUInt32);

    // LDW
    valText.setValue(std::string("Active"));
    setPropertyValue("ldw.left_line_state", valText);
    setPropertyValue("ldw.right_line_state", valText);

    // SetSpeed
    valUInt32.setValue(static_cast<uint32_t>(40));
    setPropertyValue("set_speed.value", valUInt32);
    valBool.setValue(true);
    setPropertyValue("set_speed.is_valid", valBool);

    // BlinkSignal
    valBool.setValue(true);
    setPropertyValue("blink_signal", valBool);
    // Enabled
    valBool.setValue(true);
    setPropertyValue("enabled", valBool);
    valBool.setValue(m_Acc_Enabled);
    setPropertyValue("acc.enabled", valBool);
    valBool.setValue(true);
    setPropertyValue("aca.enabled", valBool);
    valBool.setValue(false);
    setPropertyValue("pacc.enabled", valBool);
    valBool.setValue(false);
    setPropertyValue("gra.enabled", valBool);
    valBool.setValue(m_Enable_Speedlimit);
    setPropertyValue("lim.enabled", valBool);
    valBool.setValue(true);
    setPropertyValue("pea.enabled", valBool);
    valBool.setValue(true);
    setPropertyValue("ldw.enabled", valBool);
    valBool.setValue(true);
    setPropertyValue("set_speed.enabled", valBool);

    sendProperties();
}

void CFasCluster::enableSpeedLimit(bool val)
{
    m_Enable_Speedlimit = val;
    AugmentedSceneTypes::Variant valBool(AugmentedSceneTypes::EVariantType::Bool, m_Enable_Speedlimit);
    setPropertyValue("lim.enabled", valBool);

    sendProperties();
}


void CFasCluster::setAccISO(bool val)
{
    m_Acc_ISO = val;
    AugmentedSceneTypes::Variant valBool(AugmentedSceneTypes::EVariantType::Bool, m_Acc_ISO);
    setPropertyValue("acc.acc_iso_state", valBool);

    sendProperties();
}

void CFasCluster::enableACC(bool val)
{
   m_Acc_Enabled = val;
   AugmentedSceneTypes::Variant valBool(AugmentedSceneTypes::EVariantType::Bool, m_Acc_Enabled);
   setPropertyValue("acc.enabled", valBool);

   sendProperties();
}

void CFasCluster::updateFasClusterACC(bool acc_state)
{
    std::string strState =  acc_state ? "Warning" : "Active";

    AugmentedSceneTypes::Variant valText(AugmentedSceneTypes::EVariantType::Text, strState);
    setPropertyValue("acc.target_vehicle_state", valText);
    setPropertyValue("acc.radar_waves_state", valText);

    sendProperties();
}

void CFasCluster::updateFasClusterLDW(bool ldw_state)
{
     std::string strState =  ldw_state ? "Active" : "Warning";

    AugmentedSceneTypes::Variant valText(AugmentedSceneTypes::EVariantType::Text, strState);
    setPropertyValue( "ldw.left_line_state", valText);
    setPropertyValue( "ldw.right_line_state", valText);
    sendProperties();
}
