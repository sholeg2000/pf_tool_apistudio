#include "Speedometer.h"
#include "augmentedscenesignals.h"

CSpeedometer::CSpeedometer(const std::string& name, std::shared_ptr<AugmentedSceneProxy<>> objRef)
    : CARSceneControl(name, objRef)
{

}

void CSpeedometer::initialize(const std::vector<AugmentedSceneTypes::PrimitiveInfo>& primitives_info)
{
   CARSceneControl::initialize(primitives_info);

   AugmentedSceneTypes::Variant valUInt32(AugmentedSceneTypes::EVariantType::UnsignedInteger32,uint32_t(m_speed));
   setPropertyValue("value", valUInt32);

   AugmentedSceneTypes::Variant valBool(AugmentedSceneTypes::EVariantType::Bool, m_speedometr);
   setPropertyValue("is_valid", valBool);
   setPropertyValue("enabled", valBool);

   sendProperties();
}


void CSpeedometer::setSpeedometerVisible(bool val)
{
   m_speedometr = val;

   AugmentedSceneTypes::Variant valBool(AugmentedSceneTypes::EVariantType::Bool, m_speedometr);
   setPropertyValue("enabled", valBool);

   sendProperties();
}

void CSpeedometer::updateSpeedValue(bool valid, int value)
{
   m_speed = value;
   m_speedometr = valid;

   AugmentedSceneTypes::Variant valUInt32(AugmentedSceneTypes::EVariantType::UnsignedInteger32,uint32_t(m_speed));
   setPropertyValue("value", valUInt32);

   AugmentedSceneTypes::Variant valBool(AugmentedSceneTypes::EVariantType::Bool, m_speedometr);
   setPropertyValue("is_valid", valBool);

   sendProperties();
}

void CSpeedometer::setSpeed(int value)
{
     updateSpeedValue(true, value);
     emit curSpeedChanged();
}
