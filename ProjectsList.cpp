#include "ProjectsList.h"
#include <QSettings>
#include <QDebug>

#include <QDir>
#include <QQmlEngine>

const QString iniFileName = "\\APIStudio.ini";
const QString sectionProjects = "Projects";
const QString keyPrjName = "Name";
const QString keyLastProject = "lastProject";

const QString keyWrpLowMatrix  = "WarpingLowMatrix";
const QString keyWrpMidMatrix  = "WarpingMidMatrix";
const QString keyWrpHighMatrix = "WarpingHighMatrix";
const QString keyWarpingCtrl   = "WarpingCtrlEnable";

CProjectData& CProjectData::operator =(const CProjectData& objRef)
{
    m_name = objRef.m_name;
    m_path = objRef.m_path;
    warpingLow = objRef.warpingLow;
    warpingMid = objRef.warpingMid;
    warpingHigh = objRef.warpingHigh;
    warpingCtrl = objRef.warpingCtrl;
    return *this;
}

CProjectData::CProjectData(const CProjectData& objRef)
    : m_name(objRef.m_name),
      m_path(objRef.m_path),
      warpingLow(objRef.warpingLow),
      warpingMid(objRef.warpingMid),
      warpingHigh(objRef.warpingHigh),
      warpingCtrl(objRef.warpingCtrl)
{

}

CProjectData::~CProjectData()
{
 //  qDebug() << "~CProjectData";
}

CProjectsList::CProjectsList(const QString& path)
    : m_appPath(path)
{

}

CProjectsList::~CProjectsList()
{
  save();
}

/* load data from persistence storage. i.e. INI file */
void CProjectsList::initialize()
{
   QSettings settings(m_appPath+iniFileName ,QSettings::IniFormat);

   int lastProject = settings.value(keyLastProject).toInt();

   int size = settings.beginReadArray(sectionProjects);

   for (int i = 0; i < size; ++i) {
       settings.setArrayIndex(i);       
       QSharedPointer<CProjectData> data = QSharedPointer<CProjectData>(new CProjectData(m_appPath,
                                                                                          settings.value(keyPrjName).toString()));
       QQmlEngine::setObjectOwnership(static_cast<QObject*>(data.data()), QQmlEngine::CppOwnership);

       // data->setProjectName(settings.value(keyPrjName).toString());
       data->setLowMatrix(settings.value(keyWrpLowMatrix).toString());
       data->setMidMatrix(settings.value(keyWrpMidMatrix).toString());
       data->setHighMatrix(settings.value(keyWrpHighMatrix).toString());
       data->setWarpingCtrlEnable(settings.value(keyWarpingCtrl).toBool());

       m_prjList.push_back(data);
   }
   settings.endArray();
\
   setCurrPrjIndx((lastProject >= size) ? 0 : lastProject);
}

void CProjectsList::save()
{
     QSettings settings(m_appPath+iniFileName ,QSettings::IniFormat);

     settings.setValue(keyLastProject, getCurrPrjIndx());

     settings.beginWriteArray(sectionProjects);

     QSharedPointer<CProjectData> data;
     for (int i = 0; i < m_prjList.size(); ++i) {

          settings.setArrayIndex(i);
          data = m_prjList[i];
          settings.setValue(keyPrjName, data->getProjectName());

          settings.setValue(keyWrpLowMatrix, data->getLowMatrix());
          settings.setValue(keyWrpMidMatrix, data->getMidMatrix());
          settings.setValue(keyWrpHighMatrix, data->getHighMatrix());
          settings.setValue(keyWarpingCtrl, data->getWarpingCtrlEnable());
     }

     settings.endArray();
     settings.sync();
}

/* return false if failed to add project. Project is created in the same directory where executable file is located */
bool CProjectsList::addProject(const QString& name)
{
   // check prj name.
    for (int i = 0; i < m_prjList.size(); ++i) {
        if (m_prjList[i]->getProjectName() == name)
        {
            qCritical() << "duplicate project name: " << name;
            return false;
        }
    }

   // create dir
    QDir curDir(m_appPath);
    curDir.mkdir(name);

    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    QSharedPointer<CProjectData> data = QSharedPointer<CProjectData>(new CProjectData(m_appPath, name));
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(data.data()), QQmlEngine::CppOwnership);

    data->setProjectName(name);
    m_prjList.push_front(data);
    setCurrPrjIndx(0);
    emit projectUpdated();

    endInsertRows();

   // save changes
    QSettings settings(m_appPath+iniFileName ,QSettings::IniFormat);
    settings.beginWriteArray(sectionProjects);

    settings.setArrayIndex(m_prjList.size()-1);
    settings.setValue(keyPrjName, name);
    settings.endArray();
    settings.sync();

  return true;
}


bool CProjectsList::setActiveProjectData(CProjectData* data)
{
    if (getCurrPrjIndx() <0 || getCurrPrjIndx() >= m_prjList.size())
    {
        qCritical() << "setActiveProjectData: Project indx is out of range: " << getCurrPrjIndx();
        return false;
    }

    *m_prjList[getCurrPrjIndx()].data() = *data;

    emit projectUpdated();

    return true;
}

CProjectData* CProjectsList::getActiveProjectData() const
{
   QSharedPointer<CProjectData>  prjData = getProjectDataByIndex(getCurrPrjIndx());

   return prjData.data();
}

QSharedPointer<CProjectData> CProjectsList::getProjectDataByIndex(int indx) const
{
    if (indx <0 || indx >= m_prjList.size())
   {
      qCritical() << "getProjectDataByIndex: Project indx out of range: "  << indx;
      return nullptr;
   }
   return  m_prjList[indx];
}

void CProjectsList::setCurrPrjIndx(int indx)
{
    m_currPrjIndx = indx;
    emit projectChanged();
}

/* QAbstractListModel implementation */
QHash<int, QByteArray> CProjectsList::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[PrjName] = "Project";
    return roles;
}

int CProjectsList::rowCount(const QModelIndex & parent) const
{
    Q_UNUSED(parent);
    return m_prjList.size();
}

QVariant CProjectsList::data(const QModelIndex & index, int role) const
{
    if (index.row() < 0 || index.row() >= m_prjList.size() )
       return QVariant(QString("data: Error!!!"));


   if (role == PrjName) {
        return getProjectDataByIndex(index.row())->getProjectName();
    }

   return QVariant(QString("TODO !!!"));
}
