#ifndef CWARPINGCONTROLADAPTER_H
#define CWARPINGCONTROLADAPTER_H

#include <QObject>
#include "ComAPIDefs.h"

#include "tproxyentry.h"
#include "warpingcontrolsignals.h"

class CWarpingControlAdapter : public TProxyEntry<WarpingControlProxy<>>
{
   public:
      CWarpingControlAdapter(std::string domain, std::string instance);

      virtual void bindQmlContext(QQmlContext* ctx) override;

      virtual void regJSInterface(QSharedPointer<QJSEngine>& jsEngine) override {} // TODO
  protected:
      CWarpingControlAdapter();

   protected:
      std::unique_ptr<CWarpingControlSignals> m_WarpingCtrlSignals;
};

#endif // CWARPINGCONTROLADAPTER_H
