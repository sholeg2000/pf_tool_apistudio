    #include "warpingcontroladapter.h"
#include "treginterfaces.h"

CWarpingControlAdapter::CWarpingControlAdapter()
   : TProxyEntry<WarpingControlProxy<>>("", "")
{

}

CWarpingControlAdapter::CWarpingControlAdapter(std::string domain, std::string instance)
    : TProxyEntry<WarpingControlProxy<>>(domain, instance)
{
}

void CWarpingControlAdapter::bindQmlContext(QQmlContext* ctx)
{
  m_WarpingCtrlSignals = std::unique_ptr<CWarpingControlSignals>(new CWarpingControlSignals(this->m_proxy));
  if (ctx != nullptr)
      ctx->setContextProperty(instWarpingControl, &(*m_WarpingCtrlSignals));
}
