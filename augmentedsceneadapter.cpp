#include "augmentedsceneadapter.h"

#include "treginterfaces.h"
#include <QQmlEngine>
CAugmentedSceneAdapter::CAugmentedSceneAdapter()
    : TProxyEntry<AugmentedSceneProxy<>>("", "")
{

}

CAugmentedSceneAdapter::CAugmentedSceneAdapter(std::string domain, std::string instance)
    : TProxyEntry<AugmentedSceneProxy<>>(domain, instance)
{

}

void CAugmentedSceneAdapter::bindQmlContext(QQmlContext* ctx)
{
    m_AugmentedSceneSignals = QSharedPointer<CAugmentedSceneSignals>(new CAugmentedSceneSignals(this->m_proxy));
    if (ctx != nullptr)
    {
        m_AugmentedSceneSignals->registerQMLInterface(ctx);
        ctx->setContextProperty(instAugmScene, &(*m_AugmentedSceneSignals));
    }
}

void CAugmentedSceneAdapter::regJSInterface(QSharedPointer<QJSEngine>& jsEngine)
{
    m_AugmentedSceneSignals->registerJSInterface(jsEngine);

    /*
    QJSValue jsItem = jsEngine->newQObject(&(*m_AugmentedSceneSignals));

    // protect object from Garbage Collector
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsItem.toQObject()), QQmlEngine::CppOwnership);

    jsEngine->globalObject().setProperty(instAugmScene, jsItem);
    */
    //qDebug() << "New AugmentedScene: " << jsEngine->globalObject().property(instAugmScene).toQObject();
}
