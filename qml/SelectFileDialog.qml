import QtQuick 2.6
import QtQuick.Dialogs 1.2


// Select File Name of WarpingMatrix
FileDialog {
   id: fileDialog
   title: "Please choose a Warping matrix file"
   //folder: shortcuts.home
   nameFilters: ["Warping Matrix files (*.awm)" ]

   enum EWarpingMatrix {
       ENONE,
       ELOW,
       EMID,
       EHIGH
   }
   property int selMatrix : 0;// EWarpingMatrix.ENONE
   property string fileName  : ""

   signal selectedMatrixFile(string fileName, int typeMatrix)

   onAccepted: {
     fileName = fileDialog.fileUrl
     fileName = fileName.replace("file:///", "")
     selectedMatrixFile(fileName, selMatrix)
      Qt.quit()
    }

   onRejected: {  Qt.quit() }
}
