import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls 2.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1

import "controls" as MyControls
import Apostera.APIStudio 1.0

ApplicationWindow {
    id : mainApp;
    visible: true
    color: bgColor
    width: 1100
    height: 700
    minimumHeight: 700
    maximumHeight: 700
    minimumWidth: 1100
    maximumWidth: 1100
    title : { (prjSettings.currProjID !== -1) ? qsTr("Apostera APIStudio - ") + prjSettings.getActiveProjectData().prjName  : qsTr("Apostera APIStudio")  }

    property color bgColor: "lightgrey"
    readonly property int itemMargin : 5

    property int carSpeed : Speedometer.curSpeed


    property bool isDiscovery : false  /* true - RendererEngine, AugmentedARScene, AugmentedScene are connected */
    property bool enableUICtrls :  false /* true - enable UI controls after initialization od CommonAPI Services */
    property bool isScriptOn : false   /* true - run script by Timer */
    property bool showRuntimeParamsPane : true /* show/hide */

    // Timer
    Item {
     Timer {// Check RenderedEngine availability
         interval: 500; running: true; repeat: true
         onTriggered: {

                       if (isScriptOn)
                             HeartBeatTimer.execJS()

                       if (AugmentedScene.isAvailable() !== isDiscovery)
                        {
                           if (isDiscovery)
                           {
                               PrimitivesModel.clearModel()
                           }
                           isDiscovery = AugmentedScene.isAvailable()
                           statRenderer.text = isDiscovery ? "Services status: Connected" : "Services status: Discovering"
                        }

                        AugmentedScene.checkConnection(PrimitivesModel)
                        AugmentedARScene.checkConnection(PrimitivesModel)

                        enableUICtrls = AugmentedScene.isAvailable() && AugmentedARScene.isAvailable()
                     }
     }
    }

    header : ToolBar {
     id : appToolBar
     RowLayout {
         anchors.fill: parent
         anchors.margins: spacing

         MyControls.MyToolButton { // Show/Hide "Projects" Pane
             text : qsTr("Projects...")
             property var prjView : null

             onClicked: { // Switch between: Projects View - Main Property pages
               if (prjView === null) {
                   prjView = prjViewComponent.createObject(parent);
                }
                if (stack.depth === 1) {
                    stack.push(prjView)
                 }  else {
                   stack.pop()
                 }
             }
         }

         MyControls.MyToolButton { // Save changes
             text: qsTr("Save")
             onClicked: {
                 JSModel.save(); // save JScripts
             }
         }

         ToolSeparator {}
         Item { Layout.fillWidth: true}

         MyControls.MyToolButton
         {
           source : isScriptOn ? "pause.png" : "play.png"
           enabled: enableUICtrls
           Layout.preferredWidth: 32
           Layout.preferredHeight: 32
           onClicked: {
               if (!isScriptOn)
                   InitEnvironment.execJS()  /* call JS that perfroms initialization */
               isScriptOn = !isScriptOn
           }
         }

         Item { Layout.fillWidth: true }
         ToolSeparator {}

         MyControls.MyToolButton
         {
           source :  "clear.png"
           Layout.preferredWidth: 32
           Layout.preferredHeight: 32
           onClicked: {
               TraceViewModel.clearTrace()
           }
         }

         ToolButton {
             text: ">>"
             Layout.preferredHeight: 32
             onClicked: {

                  console.log("splitView.width=" + splitView.width, "mainProperties.width="+mainProperties.width, "Primitives.width="+viewPrimitives.width)

                 showRuntimeParamsPane = !showRuntimeParamsPane
                 text = (showRuntimeParamsPane) ? ">>" : "<<"

                 // mainProperties.
             }
         }

     }
 }

    /* Application StatusBar.  */
    footer: StatusBar {
       id : appStatusBar
       RowLayout {
         anchors.fill: parent
          Label {
             id : statRenderer
             text: "Rendered status: Discovering..."
          }
       }
     }// statusBar

     StackView {
        id: stack
        initialItem: splitView
        anchors { top : appToolBar.bottom; bottom : appStatusBar.top; margins: itemMargin;  }
        width: parent.width
        focus: true
      }

    // ---------------- Main ViewSection -----------
    SplitView {  // Main Pane contains Split View.
       id : splitView
       orientation: Qt.Horizontal
       property var ratio: [ 0.7, 0.3 ]


       Rectangle { // Left View with Controls
          id : mainProperties

          property int saveWidth : splitView.width
          width : (showRuntimeParamsPane) ? saveWidth*splitView.ratio[0] : saveWidth
          border.color: "DarkGrey"
          color: bgColor
          Layout.fillWidth : true
          Layout.minimumWidth: splitView.width*0.5

          onWidthChanged: {
              console.log("Width changed. resizing=" + splitView.resizing)
               //if (splitView.resizing  )
               //    splitView.ratioChanged()
           }

          TabView {
              anchors.topMargin: 4
              height:mainProperties.height
              width: mainProperties.width

             Tab {
                title : "UI Controls"

               Rectangle
               {
                 id: idRectCtrl
                 border.color: "DarkGrey"
                 color: bgColor

                 Frame { // WarpingControl.   [OS] Not supported
                    id: warpingCtrl
                    enabled : false // enableUICtrls
                    anchors {  top : idRectCtrl.top; left : idRectCtrl.left; right: idRectCtrl.right; margins: itemMargin }
                    height : appToolBar.height*2
                    Label {
                      id: idWrpTxt;
                      text: qsTr("WarpingControl attributes:")
                     }
                    ColumnLayout { // Change mirror position
                       anchors { left : parent.left; top: idWrpTxt.bottom; right: parent.right; margins: itemMargin }
                       RowLayout { // Mirror position
                       Label { text: qsTr("Mirror position: ") }
                       Slider {
                          value: 0
                          stepSize: 0.04
                          from : -2; to : 2

                          onMoved:
                          {
                             WarpingControl.updateMirrorPosition(value)
                             mirrorPos.text = value.toFixed(2)
                          }
                       }
                       Label { id: mirrorPos; }
                     }
                  }
                }

                 Frame { // AugmentedScene
                   id: augmentedScene
                   enabled : enableUICtrls
                   anchors {  top : warpingCtrl.bottom; left : parent.left; right: idRectCtrl.right; margins: itemMargin }
                   height : appToolBar.height*6
                   Label {
                     id: idAugScnTxt
                     text: qsTr("Augmented Scene (Status Line) attributes:")
                   }
                   ColumnLayout {
                      anchors { left : parent.left; top: idAugScnTxt.bottom; right: parent.right; margins: itemMargin }
                      RowLayout { // Speed Value
                        Label {  text: qsTr("Speed value:") }
                        Slider {
                           value: carSpeed
                           stepSize: 1
                           from : 0; to : 400
                           onMoved: Speedometer.setSpeed(value)
                      }
                    Label { id: speedPos; text : carSpeed }
                  }
                      RowLayout { // Fas Cluster ACC
                         Label {  text: qsTr("Fas Cluster ACC:") }
                         RadioButton {
                           text: "Warning"
                           onClicked: FASCluster.updateFasClusterACC(true)
                         }
                         RadioButton {
                           checked:true
                           text: "Active"
                           onClicked: FASCluster.updateFasClusterACC(false)
                         }
                      }
                      RowLayout { // Fas Cluster LDW
                        Label {  text: qsTr("Fas Cluster LDW:") }

                        RadioButton {
                          text: "Warning"
                          onClicked: FASCluster.updateFasClusterLDW(false)
                        }
                        RadioButton {
                          text: "Active"
                          checked: true
                          onClicked: FASCluster.updateFasClusterLDW(true)
                        }
                      }
                      RowLayout { // Time gap
                        enabled: false   /* Not Supported */
                        CheckBox {
                          id : chkbTimeGap
                          text: "Time gap enabled"
                        }

                        Label {  text: qsTr("Level:") }
                        Slider {
                           value: 0 ; stepSize: 1; from : 0; to : 5

                           onMoved: {
                              AugmentedScene.updateTimeGap(chkbTimeGap.checked, value.toFixed());
                              tgLevel.text = value
                           }
                        }
                        Label { id: tgLevel; text : "0" }
                     }
                   }
                 }

                 Frame {  // AugmentedAR Scene
                   id: augmentedARScene
                   enabled : enableUICtrls
                   anchors {  top : augmentedScene.bottom; left : parent.left; right: idRectCtrl.right; margins: itemMargin }
                   height :  appToolBar.height*6
                   Label {
                      id: idAugScnARTxt
                      text: qsTr("AugmentedAR Scene attributes")
                   }
                   ColumnLayout {
                       anchors { left : parent.left; top: idAugScnARTxt.bottom; right: parent.right; margins: itemMargin }
                       RowLayout { // update ACC
                         CheckBox {
                            id : chkbACCEnable
                            text: "ACC enable"

                            onClicked: {
                                               ACC.enable = chkbACCEnable.checked
                                             }
                         }
                       RadioButton {
                         id : idACCState
                         checked:true
                         text: "Warning"
                         onClicked: ACC.showWarning(true)
                       }
                       RadioButton {
                          text: "Active"
                          onClicked: ACC.showWarning(false)
                       }
                }
                       RowLayout { // Right and Left LDW
                   GroupBox { // Left LDW
                    id: ctrlLeft
                    title: qsTr("Left LDW")

                    RowLayout{
                      anchors.fill : parent
                      RadioButton {
                        checked:true
                        id : leftLDW
                        text: "Normal"
                        onClicked:  LDW.updateLDW(false, rightLDW.checked) }
                      RadioButton {
                        text: "Warning"
                        onClicked: LDW.updateLDW(true, rightLDW.checked) }
                    }
                   }
                   GroupBox { // Right LDW
                    id: ctrlRight
                    title: qsTr("Right LDW")
                    RowLayout {
                       anchors.fill : parent
                       RadioButton {
                         checked:true
                         text: "Normal"
                         onClicked:  LDW.updateLDW(!leftLDW.checked, false)
                       }
                       RadioButton {
                           id : rightLDW
                           text: "Warning"
                           onClicked:   LDW.updateLDW(!leftLDW.checked, true)
                       }
                    }
                }
            }
                       RowLayout { // Camera
              Label { text: qsTr("Camera position Y: ") }
              Slider {
                  value: 0
                  stepSize: 1
                  from : 0
                  to : 90

                  onMoved:
                  {
                      AugmentedARScene.updateCamera(value)
                      cameraPos.text =  value
                  }
              }
              Label { id: cameraPos; }
                       }
                  }
                }
               }
             }

             Tab { /* Scripts View */
                 title: "Scripts"

                 ScriptsView {
                     id : idScriptsView
                     border.color: "DarkGrey"
                     color: bgColor
                 }
             }
          }

        }

      // ColumnLayout {
      //    spacing: 2
          PrimitivesView {  // PrimitivesView
            id : viewPrimitives
           // width: (showRuntimeParamsPane) ? splitView.width*0.3 : 0
            width: (showRuntimeParamsPane) ? mainProperties.saveWidth*splitView.ratio[1]: 0
            height:splitView.height
            color: "mintcream" // "ghostwhite"
            anchors.margins: 10
            border.color: "DarkGrey"
          }
     // }
    }
    // --------END MAIN VIEW-------------------------

    /* Display View with projects. Add, Remove, Edit ptojects */
    Component
    {
       id: prjViewComponent

       ProjectsView
       {

       }
    }

    Component.onCompleted:
    {
        // Init project data
        var prjData = prjSettings.getActiveProjectData()
        if (prjData  !== "undefined")
        {
           JSModel.loadData(prjData)
        }
    }
}
