/* Display list of the projects. Provide functionality to
  - add, delete project
  - make initial settings
*/
import QtQuick 2.6
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

import Apostera.APIStudio 1.0

Rectangle {
   id : root
   color: "transparent"
   clip: true
   readonly property int itemMargin : 5

   readonly property int csElideWidth : 420;
   readonly property string csFontName : "Arial"

   property CProjectData prjData : null


   SelectFileDialog { /* Select WarpingMatrix FileName */
     id : fileDlg

     onSelectedMatrixFile:
     {
        switch (typeMatrix)
        {
           case SelectFileDialog.EWarpingMatrix.ELOW :
               prjData.lowMatrix = fileName
               prjSettings.setActiveProjectData(prjData)
               idLowMatrix.text = fileName;
            break;
           case SelectFileDialog.EWarpingMatrix.EMID :
               prjData.midMatrix = fileName
               prjSettings.setActiveProjectData(prjData)
               idMidMatrix.text = fileName;
           break;
           case SelectFileDialog.EWarpingMatrix.EHIGH :
               prjData.highMatrix = fileName
               prjSettings.setActiveProjectData(prjData)
               idHighMatrix.text = fileName;
           break;
        }
     }
   }

   NewProject { // Add new project
       id : dlgNew
       onNameEntered:
       {
           prjSettings.addProject(name)
           prjData = prjSettings.getActiveProjectData()
       }
   }

   RowLayout {
      anchors.fill: parent
      spacing: 0


      Rectangle { // Left Pane: List of Projects
          Layout.fillHeight: true
          Layout.preferredWidth: 200
          width : 100
          color: "whitesmoke"
          border { width: 1; color: "black" }

          Label {
              id : lblRecentPrj
              font.family: "Helvetica"
              font.pointSize: 10
              anchors {
                  top: parent.top
                  left: parent.left
                  margins: itemMargin
              }
              text : "Recent projects:"
          }

          ListView { // Display list of projects
              id: lstRecentPrj
              anchors {
                  top: lblRecentPrj.bottom
                  left: parent.left
                  margins: itemMargin
                  leftMargin: 10
              }

              height  : 200
              width   : 180
              model   : prjSettings
              delegate: dbDelegate;
              focus   : true
              currentIndex : prjSettings.currProjID

              onCurrentIndexChanged: { // selected Project changed: Save changes and load new
                  if (prjSettings.currProjID !== currentIndex)
                  {
                      console.log("TODO: Change Project. Indx=" + currentIndex)
                      JSModel.save()   // save JScript
                      prjSettings.save() // save project

                      prjSettings.currProjID = currentIndex
                      prjData = prjSettings.getActiveProjectData()
                      JSModel.loadData(prjData)
                      prjSettings.currProjID = currentIndex // need to update UI
                      prjData.refresh()
                  }
              }

              Component {
                 id: dbDelegate
                 Rectangle {
                   id: wrapper
                   width: 180

                   height: contactInfo.height
                   color: wrapper.ListView.isCurrentItem ? "lightsteelblue" : "whitesmoke";
                   Text {
                     id: contactInfo
                     text : Project
                     color: wrapper.ListView.isCurrentItem ? "blue" : "black"
                   }
                   MouseArea {
                        anchors.fill: parent
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        onClicked: {
                           lstRecentPrj.currentIndex = index

                          /*  if (mouse.button === Qt.RightButton)
                            {
                               menuProj.popup()
                            } */
                         }
                      }
                  }
               }
          }
        }

      Rectangle { // Right Pane: Open or Create New Project. Eddit Project parameters: i.e. Warping Matrix
          id: rhPane
          Layout.fillHeight: true
          Layout.fillWidth: true
          color: "lightgray"

          Button { // Import existing project
              id : btOpenPrj
              anchors {
                  top: parent.top
                  left: parent.left
                  margins: itemMargin
              }

              text: "Import project.."
              onClicked: { console.log("TODO: Add existent project") }
          }

          Button { // Create New Project
              id : btCreatePrj
              anchors {
                  top: parent.top
                  left: btOpenPrj.right
                  margins: itemMargin
              }

              text: "New project..."
              onClicked: {
                   console.log("Create New Project") // newProject.show()
                  dlgNew.open()
              }
          }

          Rectangle {  // Line to devide space
              id: divider
              height: 2
              width: parent.width
              color: "DarkGreen"
              anchors { top: btCreatePrj.bottom;  margins: itemMargin }
          }

          Frame { // WarpingControl
            id: warpingCtrlPrp
            enabled: (prjData != null) ? true : false

            anchors {  top : divider.bottom; left : divider.left; right: divider.right; margins: itemMargin }
            height : btOpenPrj.height*5.5

            CheckBox {
                id: chckWarpingCtrl
                checked : (prjData == null) ? false: prjData.enableWarpingCtrl
                text: "Warping Control Interface"
                onCheckStateChanged: {
                    if (prjData)
                        prjData.enableWarpingCtrl = checked
                }
            }

            ColumnLayout {
               anchors { left : parent.left; top: chckWarpingCtrl.bottom; right: parent.right; margins: itemMargin }
               enabled: chckWarpingCtrl.checkState

                   Label {
                       id: idWrpTxt
                       text: qsTr("WarpingControl attributes:")
                   }

               RowLayout { // LOW Matrix
                   Button {
                       text: "Low matrix..."
                       onClicked: {
                           fileDlg.selMatrix = SelectFileDialog.EWarpingMatrix.ELOW
                           fileDlg.visible = true
                       }
                   }

                   TextMetrics {
                       id: tmLow
                       font.family: csFontName
                       elide: Text.ElideMiddle
                       elideWidth: csElideWidth
                       text: { (prjData == null) ? "" : prjData.lowMatrix }
                   }

                   Text { id: idLowMatrix; text: tmLow.elidedText }
               }
               RowLayout { //
                   Button {
                       text: "Mid matrix..."
                       onClicked: {
                           fileDlg.selMatrix = SelectFileDialog.EWarpingMatrix.EMID
                           fileDlg.visible = true
                       }
                   }

                   TextMetrics {
                       id: tmMid
                       font.family: csFontName
                       elide: Text.ElideMiddle
                       elideWidth: csElideWidth
                       text: { (prjData == null) ? "" : prjData.midMatrix }
                   }

                   Text { id: idMidMatrix; text : tmMid.elidedText }
               }
               RowLayout {
                   id : idRowHigh
                   Button {
                       text: "High matrix..."
                       onClicked: {
                           fileDlg.selMatrix = SelectFileDialog.EWarpingMatrix.EHIGH
                           fileDlg.visible = true
                       }
                   }

                   TextMetrics {
                       id: tmHigh
                       font.family: csFontName
                       elide: Text.ElideMiddle
                       elideWidth: csElideWidth
                       text: { (prjData == null) ? "" : prjData.highMatrix }
                   }

                   Text { id: idHighMatrix; text: tmHigh.elidedText }
               }
            }
        }
      }

  }

   Component.onCompleted: prjData = prjSettings.getActiveProjectData()
}
