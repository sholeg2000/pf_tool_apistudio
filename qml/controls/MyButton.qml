import QtQuick 2.12
import QtQuick.Layouts 1.12


Item {
    id: root

    property string text
    property color bgColor: "#757575"
    property color bgColorSelected: "#bdbdbd"
    property color textColor: "white"
    property color textColorSelected: "black"
    property color textColorDisable: "gainsboro"
    property alias radius: bgr.radius
    property alias textHorizontAlign: text.horizontalAlignment

    property alias source: img.source
    property int size: 32

    signal clicked

     Rectangle {
        id: bgr
        height: 30
        width: 100
        anchors.fill: parent
        color: mouseArea.pressed ? bgColorSelected : bgColor
        radius: height / 15

        property bool stateCheck : true
        Image{
          id: img
          anchors.fill : parent
          width: size
          height: size
          mipmap: true
          horizontalAlignment: Image.AlignLeft
          fillMode: Image.PreserveAspectFit

        }

        Text {
            id: text
            anchors.centerIn: parent
            text: root.text
            font.pixelSize: 0.4 * parent.height
            color: {
                    if (!enabled) { textColorDisable }
                      else if (mouseArea.pressed) { textColorSelected } else textColor
                   }
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            onClicked: {
                root.clicked()
            }
        }
    }
}
