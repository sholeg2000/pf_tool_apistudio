import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.1

ToolButton {
  property alias source: img.source
  property int size: 32
  property bool stateCheck : true
  Image{
    id: img
    width: size
    height: size
    mipmap: true
    fillMode: Image.PreserveAspectFit
  }
  //Layout.preferredHeight: size
  //Layout.preferredWidth: size

  background: Rectangle {
        implicitWidth: 100
        implicitHeight: 30
        border.width: activeFocus ? 2 : 1
        border.color: "#888"
        radius: 4
        gradient: Gradient {
          GradientStop { position: 0 ; color: pressed ? "#ccc" : "#eee" }
          GradientStop { position: 1 ; color: pressed ? "#aaa" : "#ccc" }
        }
    }
}
