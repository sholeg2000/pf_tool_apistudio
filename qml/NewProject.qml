import QtQuick 2.6
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.1
import "controls" as MyControls

Popup {
    id: popup

    signal nameEntered(string name)

    background: Rectangle {
        implicitWidth: 350
        implicitHeight: 150
        border.color: "#444"
        border.width: 2
        color: "lightgray"
    }

    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent


    parent: Overlay.overlay
    x: Math.round((parent.width - width) / 2)
    y: Math.round((parent.height - height) / 2)
    width: 350
    height: 150


    ColumnLayout {
       anchors.fill: parent
       Frame {
           RowLayout {
              Label { text: "Project Name: "}
              TextField {
                  id : prjEdit
                  width: 100
                  focus: true
              }
           }
       }
       RowLayout {
            MyControls.MyButton {
                id : btOK
                height: 30
                width: 100
                text: "OK"
                onClicked: {
                    popup.nameEntered(prjEdit.text)
                    close()
                }
            }

            MyControls.MyButton{
                id : btCancel
                height: 30
                width: 100
                text : "Cancel"
                onClicked: { close() }
            }
        }
    }
}
