import QtQuick 2.12

import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4
import QtQuick.Controls 2.12

Rectangle {
   id : root
   color: "transparent"
   clip: true

   Column
   {
       anchors.fill: parent
       spacing: 2

       TreeView {
       id : idPrim
       width: parent.width
       height: parent.height*0.5
       selectionMode: SelectionMode.SingleSelection

       TableViewColumn {
           title: "Primitive"
           role: "primitiveName"
           width: idPrim.width*0.7
       }
       TableViewColumn {
           title: "Value"
           role: "propValue"
           width: idPrim.width*0.3
       }
       model: PrimitivesModel

  /*     onCurrentIndexChanged: {
       }*/
   }

       TableView {
           id: tableView

           width: parent.width
           height: root.height*0.5
           frameVisible: true

           alternatingRowColors : true
           selectionMode: SelectionMode.SingleSelection

           model: TraceViewModel

          TableViewColumn {
               id: primitiveCol
               title: "Primitive"
               role: "Primitive"
               movable: false
               resizable: true
               width: 100
           }

           TableViewColumn {
               id: attrCol
               title: "Attribute"
               role: "Attribute"
               movable: false
               resizable: true
               width: 100
           }

           TableViewColumn {
               id: valueColumn
               title: "Value"
               role: "Value"
               movable: false
               resizable: true
               width: tableView.viewport.width - primitiveCol.width - attrCol.width
           }
       }
   }
}
