import QtQuick 2.6

import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4
import QtQuick.Controls 2.12

Rectangle {
   id : root
   color: "transparent"
   clip: true

   TreeView {
       id : idObjProp
       width: parent.width
       height: parent.height
       selectionMode: SelectionMode.SingleSelection

       TableViewColumn {
           title: "Object Propetries"
           role: "propertyName"
           width: idObjProp.width
       }
      // model: ObjPropView

       onCurrentIndexChanged: {
       }
   }
}

