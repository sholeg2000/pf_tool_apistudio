import QtQuick 2.6
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4
import QtQuick.Controls 2.12

import Apostera.APIStudio 1.0
import "controls" as MyControls

Rectangle {
   id : root
   color: "transparent"
   clip: true

   readonly property int itemMargin : 5
   property color bgrPaneColor : "gainsboro"
   property color borderPaneColor:"grey"
   property CJSItem jsItem: null

   Row {
       anchors.fill: parent
       spacing: 2

       Rectangle {
           id : leftPane
           color: bgrPaneColor
           border { width: 1; color: borderPaneColor}
           width: parent.width * 0.3
           anchors {  top: parent.top; bottom: parent.bottom }

           Column {
              anchors.fill: parent
              TreeView {
                  id : idJSTree
                  width: parent.width
                  height: parent.height*0.6
                  selectionMode: SelectionMode.SingleSelection

                  TableViewColumn {
                      title: "Javascript Module/Handler"
                      role: "moduleName"
                      width: idJSTree.width
                  }
                  model: JSModel

                  onCurrentIndexChanged: {
                      if (jsItem)
                          jsItem.jsBody = editJS.text

                       if (currentIndex.row >=0) {
                          jsItem = JSModel.selectItem(currentIndex, currentIndex.row, prjSettings.getActiveProjectData())

                          idDesc.text = jsItem.itemDescription
                       }
                  }
              }

              TextEdit { // Context description of selected item
                  id : idDesc
                  wrapMode: TextEdit.WordWrap
                  width: parent.width
                  height: parent.height*0.4
                  readOnly: true
                  // elide : Text.ElideLeft
                  text: ""
              }
           }
       }

       Rectangle {
          id : rightPane
          color: bgrPaneColor
          width: parent.width * 0.7
          height: parent.height
          border { width: 1; color:  borderPaneColor }

          Column {
             anchors.fill: parent
             spacing: 10
             Rectangle {
               id: rectEdit
               height: rightPane.height*0.8
               width: parent.width
               color: bgrPaneColor
               border { width: 1; color: borderPaneColor }

               Flickable {
                  id: flick
                  width: rectEdit.width; height: rectEdit.height;
                  contentWidth: editJS.paintedWidth
                  contentHeight: editJS.paintedHeight
                  clip: true

                  function ensureVisible(r)
                  {
                    if (contentX >= r.x)
                       contentX = r.x;
                    else if (contentX+width <= r.x+r.width)
                        contentX = r.x+r.width-width;
                   if (contentY >= r.y)
                        contentY = r.y;
                   else if (contentY+height <= r.y+r.height)
                            contentY = r.y+r.height-height;
                 }

                 TextEdit {
                   id : editJS
                   enabled: (jsItem) ? jsItem.isJSHandler() : false
                   wrapMode : TextEdit.Wrap
                   width: flick.width
                   font { family: "Helvetica" ; pointSize: 10 }
                   focus: true

                   text: (jsItem) ? jsItem.jsBody : ""
                   onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)
                   onEditingFinished: { jsItem.jsBody = (jsItem) ? editJS.text : null }
                }
               }
             }

             MyControls.MyButton
             {
                id : btValidate
                enabled: (jsItem) ? jsItem.isJSHandler() : false
                source : "validate.png"
                text : "Validate"
                textHorizontAlign: Text.AlignRight
                anchors.left: parent.left
                anchors.leftMargin: 10
                radius: 2
                height: 35
                width : 120
                onClicked: {
                    jsItem.jsBody = editJS.text
                    jsItem.valudateJS()
                }
              }
             Label {
               text: (jsItem) ? jsItem.jsValidationText : "<JavaScript validation result..>"
               anchors.leftMargin: 20
             }
        }
    }
   }
}
