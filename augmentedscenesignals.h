#ifndef AUGMENTEDSCENESIGNALS_H
#define AUGMENTEDSCENESIGNALS_H

#include <QObject>
#include <QSharedPointer>

#include <QQmlEngine>

#include "Speedometer.h"
#include "FasCluster.h"

class CPrimitivesModel;

class CAugmentedSceneSignals : public QObject
{
   Q_OBJECT
public:
    CAugmentedSceneSignals(QObject *parent = nullptr);
    CAugmentedSceneSignals(std::shared_ptr<AugmentedSceneProxy<>> objRef);
    virtual ~CAugmentedSceneSignals() override;

    void registerQMLInterface(QQmlContext* ctx);
    void registerJSInterface(QSharedPointer<QJSEngine>& jsEngine);

    // shoould be called after estableshing connection.
    void setupStatusLineSZoneScene(CPrimitivesModel* model);

    // make initialization when Proxy becomes available
    Q_INVOKABLE void checkConnection(CPrimitivesModel* model);


public slots:
     bool isAvailable() const { return m_connected; }

      Q_DECL_DEPRECATED void updateTimeGap(bool enabled, int level);
public:

protected:
      void clearSceneObjects();

protected:
    std::shared_ptr<AugmentedSceneProxy<>>  m_proxy;
    CSpeedometer m_ctrlSpeedometer;
    CFasCluster  m_ctrlFusCluster;

    bool m_connected = false;
};

#endif // AUGMENTEDSCENESIGNALS_H
