#include <Winsock2.h>
#include <windows.h>

#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QNetworkInterface>


#include <QtQuick/qquickview.h>

#include <QDebug>

#include "treginterfaces.h"
#include "ProjectsList.h"
#include "JSModules.h"
#include "JSItem.hpp"
#include "PrimtivesModel.h"
#include "TraceViewModel.h"

void checkLocalIPAddress()
{

 // int indx = QNetworkInterface::index();
  //QNetworkInterface inf = QNetworkInterface::interfaceFromIndex(indx);


  QList<QHostAddress> list = QNetworkInterface::allAddresses();

 for(int nIter=0; nIter<list.count(); nIter++)

  {
      if(!list[nIter].isLoopback())
          if (list[nIter].protocol() == QAbstractSocket::IPv4Protocol )
        qDebug() << list[nIter].toString();

  }
}


int main(int argc, char *argv[])
{
    _putenv_s("VSOMEIP_APPLICATION_NAME", "hmiclient");
    _putenv_s("VSOMEIP_CONFIGURATION", "asclient.json");


    checkLocalIPAddress();

    QGuiApplication app(argc, argv);

    QQuickView view;
    QQmlContext *ctxt = view.rootContext();

   QSharedPointer<QJSEngine> jsEngine = QSharedPointer<QJSEngine>(new QQmlEngine(ctxt));

    /* --- Initialize and Run CommonAPI Interfaces --- */
    CCommInterfaces comIF;
    comIF.InitializeComAPI(ctxt, jsEngine);

    /* --- Recent Projects List --- */
    CProjectsList  prjSettings(QGuiApplication::applicationDirPath());
    prjSettings.initialize();
    ctxt->setContextProperty("prjSettings", &prjSettings);

    /* --- TreeView Model for JavaScript Units --- */
    CJSModules jsModules(jsEngine);
    jsModules.createModel(ctxt);
    ctxt->setContextProperty("JSModel", &jsModules);  /* Register Model for TreeView */

    /* --- Property TreeView Model --- */
    CPrimitivesModel propModel;
    ctxt->setContextProperty("PrimitivesModel", &propModel);

    /* TraceView */
   CTraceViewModel   traceViewModel;
    ctxt->setContextProperty("TraceViewModel", &traceViewModel);

    QObject::connect(&propModel, &CPrimitivesModel::outputValue, &traceViewModel, &CTraceViewModel::addValue);

    /* --- Register custom types --- */
    qmlRegisterType<CProjectData>("Apostera.APIStudio", 1, 0, "CProjectData");
    qmlRegisterType<CJSItem>("Apostera.APIStudio", 1, 0, "CJSItem");
    qmlRegisterType<CPrimitivesModel>("Apostera.APIStudio", 1, 0, "CPrimitivesModel");


    view.setSource(QUrl("qrc:/qml/main.qml"));

    return app.exec();
}
