#ifndef CAACC_H
#define CAACC_H

#include "ARSceneControl.h"

class CAcc : public  CARSceneControl
{
    Q_OBJECT
public:
    CAcc(const std::string& name, std::shared_ptr<AugmentedSceneProxy<>> objRef);

    void initialize(const std::vector<AugmentedSceneTypes::PrimitiveInfo>& primitives_info);

    Q_PROPERTY(bool enable READ isACCEnabled WRITE enableACC)

    Q_INVOKABLE void showWarning(bool state);

protected:
    bool isACCEnabled() const { return m_Acc_Enabled; }
    void enableACC(bool val);

protected:
    CAcc() :  CAcc("Invalid", nullptr) {}

    bool m_Acc_Enabled = false;

};

#endif // CAACC_H
