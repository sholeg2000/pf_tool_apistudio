#include "augmentedscenesignals.h"
#include <QDebug>
#include <string>
#include <QtQml>

#include "AugmentedSceneCommon.hpp"
#include "PrimtivesModel.h"

CAugmentedSceneSignals::CAugmentedSceneSignals(QObject *parent)
    : QObject(parent), m_ctrlSpeedometer("Invalid", nullptr), m_ctrlFusCluster("Invalid", nullptr)
{

}

CAugmentedSceneSignals::CAugmentedSceneSignals(std::shared_ptr<AugmentedSceneProxy<>> objRef)
    : QObject(nullptr), m_proxy(objRef), m_ctrlSpeedometer("Speedometer", objRef),
      m_ctrlFusCluster("FASCluster", objRef)
{

}

CAugmentedSceneSignals::~CAugmentedSceneSignals()
{
  clearSceneObjects();
}

void CAugmentedSceneSignals::registerQMLInterface(QQmlContext* ctx)
{
  ctx->setContextProperty("Speedometer", &m_ctrlSpeedometer);
  ctx->setContextProperty("FASCluster", &m_ctrlFusCluster);
}

void CAugmentedSceneSignals::registerJSInterface(QSharedPointer<QJSEngine>& jsEngine)
{
    // Speedometer
   QJSValue jsItem = jsEngine->newQObject(&m_ctrlSpeedometer);

   // protect object from Garbage Collector
   QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsItem.toQObject()), QQmlEngine::CppOwnership);
   jsEngine->globalObject().setProperty("Speedometer", jsItem);

   // FASCluster
    jsItem = jsEngine->newQObject(&m_ctrlFusCluster);

   // protect object from Garbage Collector
   QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsItem.toQObject()), QQmlEngine::CppOwnership);
   jsEngine->globalObject().setProperty("FASCluster", jsItem);

}

void CAugmentedSceneSignals::checkConnection(CPrimitivesModel* model)
{
  if (!m_connected && (m_proxy && m_proxy->isAvailable()))
  {
   if (model)
   {
       setupStatusLineSZoneScene(model);
   }
  }

  m_connected = (m_proxy && m_proxy->isAvailable());
}

void CAugmentedSceneSignals::setupStatusLineSZoneScene(CPrimitivesModel* model)
{
   std::vector<AugmentedSceneTypes::PrimitiveInfo> primitives_info;

    CommonAPI::CallStatus call_status;
    m_proxy->getSupportedPrimitives(call_status, primitives_info);

    model->addPrimitives(primitives_info); // Add Primitives to TreeView Model

    QObject::connect(&m_ctrlSpeedometer, &CARSceneControl::valueChanged, model, &CPrimitivesModel::setValue);
    QObject::connect(&m_ctrlFusCluster, &CARSceneControl::valueChanged, model, &CPrimitivesModel::setValue);

    m_ctrlSpeedometer.initialize(primitives_info);
    m_ctrlFusCluster.initialize(primitives_info);
}

void CAugmentedSceneSignals::clearSceneObjects()
{
    m_ctrlSpeedometer.removeObject();
    m_ctrlFusCluster.removeObject();
}

void CAugmentedSceneSignals::updateTimeGap(bool enabled, int level)
{
     qDebug() << "updateTimeGap: enable=" << enabled << " level=" << level;

   /* AugmentedSceneTypes::ObjectsProperties obj_prop = {
        {m_status_line_scene.objid,
         {
             {m_status_line_scene.acc_timegap_enabled_propid, AugmentedSceneTypes::Variant{
                 AugmentedSceneTypes::EVariantType::Bool, enabled}},
             {m_status_line_scene.acc_timegap_level_propid, AugmentedSceneTypes::Variant{
                 AugmentedSceneTypes::EVariantType::UnsignedInteger32, (uint32_t)level}},
         }},
    };


    CommonAPI::CallStatus call_status;
    m_proxy->updateObjectsProperties(obj_prop, call_status);
*/
}
