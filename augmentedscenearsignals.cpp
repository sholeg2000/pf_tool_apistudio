#include "augmentedscenearsignals.h"
#include "AugmentedSceneCommon.hpp"
#include "glm/glm.hpp"
#include <QtQml>

#include <QDebug>
#include "PrimtivesModel.h"

CAugmentedSceneARSignals::CAugmentedSceneARSignals(QObject *parent)
    : QObject(parent),  m_ctrlLDW("Invalid", nullptr ), m_ctrlACC("invalid", nullptr)
{

}

CAugmentedSceneARSignals::CAugmentedSceneARSignals(std::shared_ptr<AugmentedSceneProxy<>> objRef)
    : m_proxy(objRef), m_ctrlLDW("LDW",objRef ), m_ctrlACC("ACC", objRef)
{

}

CAugmentedSceneARSignals::~CAugmentedSceneARSignals()
{
   clearSceneObjects();
}

void CAugmentedSceneARSignals::registerQMLInterface(QQmlContext* ctx)
{
   ctx->setContextProperty("LDW", &m_ctrlLDW);
   ctx->setContextProperty("ACC", &m_ctrlACC);

   ctx->setContextProperty("AugmentedARScene", this);
   ctx->setContextProperty("CameraPosition", &(getCameraPosition()));
   ctx->setContextProperty("CameraRotation", &(getCameraRotation()));
}

void CAugmentedSceneARSignals::registerJSInterface(QSharedPointer<QJSEngine>& jsEngine)
{
    // LDW
  QJSValue jsItem = jsEngine->newQObject(&m_ctrlLDW);

   // protect object from Garbage Collector
   QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsItem.toQObject()), QQmlEngine::CppOwnership);
   jsEngine->globalObject().setProperty("LDW", jsItem);

   // ACC
   jsItem = jsEngine->newQObject(&m_ctrlACC);

    // protect object from Garbage Collector
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(jsItem.toQObject()), QQmlEngine::CppOwnership);
    jsEngine->globalObject().setProperty("ACC", jsItem);

}

void CAugmentedSceneARSignals::checkConnection(CPrimitivesModel* model)
{
  if (!m_connected && (m_proxy && m_proxy->isAvailable()))
  {
    if (model)
    {
        setupARSZoneScene(model);
    }
  }

  m_connected = (m_proxy && m_proxy->isAvailable());
}

void CAugmentedSceneARSignals::clearSceneObjects()
{
    m_ctrlLDW.removeObject();
    m_ctrlACC.removeObject();
}

bool CAugmentedSceneARSignals::setupARSZoneScene(CPrimitivesModel* model)
{
    std::vector<AugmentedSceneTypes::PrimitiveInfo> primitives_info;

    CommonAPI::CallStatus call_status;
    m_proxy->getSupportedPrimitives(call_status, primitives_info);


    model->addPrimitives(primitives_info); // Add Primitives to TreeView Model

    QObject::connect(&m_ctrlLDW, &CARSceneControl::valueChanged, model, &CPrimitivesModel::setValue);
    QObject::connect(&m_ctrlACC, &CARSceneControl::valueChanged, model, &CPrimitivesModel::setValue);

    m_ctrlLDW.initialize(primitives_info);
    m_ctrlACC.initialize(primitives_info);

    AugmentedSceneTypes::EResultType result;
    m_proxy->grabCameraAccess(call_status, result);
    if (result != AugmentedSceneTypes::EResultType::Success)
    {
        qCritical() << "Couldn't grab camera access";
    }

    updateCamera(0);

    return (result == AugmentedSceneTypes::EResultType::Success) ? true : false;
}

 void CAugmentedSceneARSignals::updateCamera(const v0::Ipc::MathTypes::Vector3& position, const v0::Ipc::MathTypes::Quaternion& rotation)
 {
     CommonAPI::CallStatus call_status;
     m_proxy->updateCamera(position, rotation, call_status);
     m_proxy->applyChanges(0, call_status);
 }

 /* Note: assumed that X,Z are constants */
 void CAugmentedSceneARSignals::updateCamera(int degreeY)
 {
     double position_var = glm::radians(degreeY*1.0);
     double position_y = std::sin(position_var);

     updateCamera(v0::Ipc::MathTypes::Vector3(0, position_y, 1.42), v0::Ipc::MathTypes::Quaternion(0.9999619, 0, 0.0087265, 0));
 }

