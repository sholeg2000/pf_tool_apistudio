#ifndef COMAPIDEFS_H
#define COMAPIDEFS_H

#include "v1/Ipc/AugmentedSceneProxy.hpp"
#include "v1/Ipc/WarpingControlProxy.hpp"

using v1::Ipc::AugmentedSceneTypes;
using v1::Ipc::AugmentedSceneProxy;
using v1::Ipc::WarpingControlProxy;
using v1::Ipc::WarpingControlTypes;

#endif // COMAPIDEFS_H
