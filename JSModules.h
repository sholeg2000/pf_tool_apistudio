#ifndef JSMODULES_H
#define JSMODULES_H

#include <QAbstractItemModel>
#include <QSharedDataPointer>

#include <QQmlContext>
#include <QJSEngine>

#include "ProjectsList.h"
#include "JSItem.hpp"

class CJSModules : public QAbstractItemModel
{
    Q_OBJECT
public:
    CJSModules();
    virtual ~CJSModules() override;
    CJSModules(QSharedPointer<QJSEngine> jsEngine);

    Q_INVOKABLE CJSItem* selectItem(const QModelIndex &index, int indx, CProjectData* prj);
    Q_INVOKABLE void loadData(CProjectData* prj);
    Q_INVOKABLE void save();

public:

    enum JSRoles {
        ModuleName = Qt::UserRole + 1,
    };

    void createModel(QQmlContext* ctxt);

    QSharedPointer<QJSEngine> getJSEngine(); /* Initilize on first call */

    QVariant data(const QModelIndex &index, int role) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;

    void loadJSModules();
protected:
  QHash<int, QByteArray> roleNames() const override;

  QSharedPointer<CJSItem> m_data;  /* Root */

  QSharedPointer<QJSEngine> m_jsEngine;

//signals:

//public slots:
};
#endif // JSMODULES_H
