#ifndef CWARPINGCONTROLSIGNALS_H
#define CWARPINGCONTROLSIGNALS_H

#include <QObject>
#include "ComAPIDefs.h"
#include <v0/Ipc/MathTypes.hpp>
#include <v1/Ipc/CommonTypes.hpp>

class CWarpingControlSignals : public QObject
{
    Q_OBJECT
public:
    CWarpingControlSignals(QObject *parent = nullptr);
    CWarpingControlSignals(std::shared_ptr<WarpingControlProxy<>> objRef);

    /* send init matrices to WarpingControl */
    Q_INVOKABLE void setupWarpingMatrices(const QString& fileLow, const QString& fileMid, const QString& fileHigh);

signals:

public slots:
   bool isAvailable() const;

   void updateMirrorPosition(double angle);
   double getMirrorPosition() const { return  m_mirror_angle; }

protected:
   WarpingControlTypes::Matrix readWarpingMatrix(uint32_t width, uint32_t height, const std::string& filename);

protected:
     std::shared_ptr<WarpingControlProxy<>>  m_proxy;
     v1::Ipc::CommonTypes::Radians m_mirror_angle = 0;
};

#endif // CWARPINGCONTROLSIGNALS_H
