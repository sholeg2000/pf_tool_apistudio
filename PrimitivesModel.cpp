#include "PrimtivesModel.h"
#include <QtQml>
#include <QDebug>

QString getPropTypeStr(AugmentedSceneTypes::EVariantType type)
{
  QString ret = "<Unknown type>";

  switch(type)
  {
    case AugmentedSceneTypes::EVariantType::UnsignedInteger32 : ret = "UInt32"; break;
    case AugmentedSceneTypes::EVariantType::Integer32 : ret = "Int32"; break;
    case AugmentedSceneTypes::EVariantType::Bool      : ret="Bool"; break;
    case AugmentedSceneTypes::EVariantType::BoolList  : ret = "BoolList"; break;
    case AugmentedSceneTypes::EVariantType::Real      : ret = "Real"; break;
    case AugmentedSceneTypes::EVariantType::RealList  : ret = "RealList"; break;
    case AugmentedSceneTypes::EVariantType::Text      : ret = "Text"; break;
    case AugmentedSceneTypes::EVariantType::TextList  : ret = "TextList"; break;
    case AugmentedSceneTypes::EVariantType::Vector2   : ret = "Vector2"; break;
    case AugmentedSceneTypes::EVariantType::Polyline2 : ret = "Polyline2";break;
    case AugmentedSceneTypes::EVariantType::Polyline2List : ret = "Polyline2List"; break;
    case AugmentedSceneTypes::EVariantType::Vector3       : ret = "Vector3"; break;
    case AugmentedSceneTypes::EVariantType::Polyline3     : ret = "Polyline3"; break;
    case AugmentedSceneTypes::EVariantType::Polyline3List : ret = "Polyline3List"; break;
    case AugmentedSceneTypes::EVariantType::Quaternion    : ret = "Quaternion"; break;
    case AugmentedSceneTypes::EVariantType::Color         : ret = "Color"; break;
  }

  return ret;
}

CPrimitivesModel::CPrimitivesModel(QObject *parent) : QAbstractItemModel(parent)
{

        m_data =  QSharedPointer<CPropItem>(new CPropItem("Root", EROOT /*Root of TreeView*/, nullptr /* no parent*/));
}

void CPrimitivesModel::clearModel()
{
    beginResetModel();
    m_data.reset();
    endResetModel();
}

void  CPrimitivesModel::setValue(const QString& primitive,const  QString& attr, const QString& value) // [OS] Experimental
{
   // Find Primitive
    CPropItem* itemPrimitive = nullptr;
    for (int i = 0 ; i < m_data->getChildsCount(); ++i)
    {
         CPropItem* item = m_data->child(i);
        if (item->getName() == primitive){
            itemPrimitive = item;
            break;
        }
    }
    if (itemPrimitive == nullptr)
    {
        qCritical() << " CPrimitivesModel::setValue Primitive not found";
        emit  outputValue("Prmitive not found: " +primitive, attr, value); // output into trace view

        return;
    }

    // Find Properties
     CPropItem* holderProp = nullptr;
    for (int i = 0 ; i < itemPrimitive->getChildsCount(); ++i)
    {
         CPropItem* item = itemPrimitive->child(i);
        if (item->getType() == EPRIM_HOLDER_PRP) {
            holderProp = item;
            break;
        }
    }
    if (holderProp == nullptr)
    {
       qCritical() << "Properties holder is not found ";
       emit  outputValue(primitive,"Property not found: " + attr, value); // output into trace view
       return;
    }

    // Find value
    QString findValue = attr + ":";
   for (int i = 0 ; i < holderProp->getChildsCount(); ++i)
   {
        CPropItem* item = holderProp->child(i);
        QString valName = item->getName();
        valName.indexOf(findValue);
       if (valName.indexOf(findValue) == 0) {

           item->setValue(value);

           QModelIndex topLeft = createIndex(i, 0, item);
           QModelIndex bottomRight = createIndex(i, 1, item);
           QVector<int> roles = QVector<int>() << PropValue << PrimitiveName;
           emit dataChanged(topLeft, bottomRight, roles);

          emit  outputValue(primitive, attr, value); // output into trace view

           return;
       }
   }

   qCritical() << "Property is not found ";
}

void CPrimitivesModel::addPrimitives(const std::vector<AugmentedSceneTypes::PrimitiveInfo>& primitives_info)
{
   if (m_data == nullptr)
   {
       m_data =  QSharedPointer<CPropItem>(new CPropItem("Root", EROOT /*Root of TreeView*/, nullptr /* no parent*/));
   }

   for (const auto& item : primitives_info)
   {
       beginInsertRows(QModelIndex(), rowCount(), rowCount());

       // Add primitive
      CPropItem* primitive = new CPropItem(QString(item.getName().data()), EPRIMITIVE, m_data.data());
      QQmlEngine::setObjectOwnership(static_cast<QObject*>(primitive), QQmlEngine::CppOwnership);

      // Add Actions
      CPropItem* holderActions = new CPropItem("Actions", EPRIM_HOLDER_ACTIONS, primitive );
      QQmlEngine::setObjectOwnership(static_cast<QObject*>(holderActions), QQmlEngine::CppOwnership);

      for (const auto& action : item.getActions())
      {
         addPropItem(holderActions, EPRIM_ACTION, QString(action.getName().data()));
      }
      if (item.getActions().size() == 0)
      {
         addPropItem(holderActions, EPRIM_ACTION, "<Not available>");
      }
      primitive->appendChild(holderActions);


      // Add Commands
      CPropItem* holderCommands = new CPropItem("Comamnds", EPRIM_HOLDER_CMNDS, primitive);
      QQmlEngine::setObjectOwnership(static_cast<QObject*>(holderCommands), QQmlEngine::CppOwnership);

      // Commands
      for (const auto& cmd : item.getCommands())
      {
         addPropItem(holderCommands, EPRIM_COMMAND, QString(cmd.getName().data()));

         // cmd.getParameters()  // [OS] TODO
      }
      if (item.getCommands().size() == 0)
      {
         addPropItem(holderCommands, EPRIM_COMMAND, "<Not available>");
      }
      primitive->appendChild(holderCommands);

      // Add Properties
      CPropItem* holderProperties = new CPropItem("Properties", EPRIM_HOLDER_PRP, primitive);
      QQmlEngine::setObjectOwnership(static_cast<QObject*>(holderProperties), QQmlEngine::CppOwnership);

      for (const auto& prop : item.getProperties() )
      {
         AugmentedSceneTypes::EVariantType type = prop.getType();
         QString poperty = QString(prop.getName().data()) + ":" + getPropTypeStr(prop.getType());
         addPropItem(holderProperties, EPRIM_PROPERTY, poperty);
      }
      if (item.getProperties().size() == 0)
      {
         addPropItem(holderProperties, EPRIM_PROPERTY, "<Not available>");
      }
      primitive->appendChild(holderProperties);

      m_data->appendChild(primitive); // Add promitive to the Model

      endInsertRows();
   }
}

void CPrimitivesModel::addPropItem(CPropItem* parentItem, EPropType type, const QString& text)
{
    CPropItem* subItem = new CPropItem(text, type, parentItem);
    QQmlEngine::setObjectOwnership(static_cast<QObject*>(subItem), QQmlEngine::CppOwnership);
    parentItem->appendChild(subItem);
}

QHash<int, QByteArray> CPrimitivesModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[PrimitiveName] = "primitiveName";
    roles[PropValue] = "propValue";
    return roles;
}

QVariant CPrimitivesModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

   QVariant retVal;
   CPropItem* item = static_cast<CPropItem*>(index.internalPointer());
   if (role == PrimitiveName)
   {
      retVal= item->getName() ;
   }else
    if (item->getType() == EPRIM_PROPERTY)
    {
        retVal = item->getValue();
    }

     return retVal;
}

int CPrimitivesModel::rowCount(const QModelIndex &parent) const
{
    if (parent.column() > 0)
        return 0;

   CPropItem* parentItem = (!parent.isValid()) ? m_data.data() : static_cast<CPropItem*>(parent.internalPointer());

   int rowCount =  (parentItem) ? parentItem->getChildsCount() : 0;

  // qDebug() << "CPrimitivesModel::rowCount=" << rowCount;
   return rowCount;
}

int CPrimitivesModel::columnCount(const QModelIndex &parent) const
{
    CPropItem* childItem = nullptr;
    if (parent.isValid())
    {
        childItem = static_cast<CPropItem*>(parent.internalPointer());
        if (childItem->getType() == EPRIM_HOLDER_PRP) return 2;
   }


    return 1;
}

QModelIndex CPrimitivesModel::index(int row, int column,  const QModelIndex &parent) const
{
   if (!hasIndex(row, column, parent))
       return QModelIndex();

   CPropItem* parentItem = nullptr;

   if (!parent.isValid())
       parentItem = m_data.data();
   else
       parentItem = static_cast<CPropItem*>(parent.internalPointer());

   CPropItem *childItem = parentItem->child(row);
   if (childItem)
       return createIndex(row, column, childItem);
   return QModelIndex();
}

QModelIndex CPrimitivesModel::parent(const QModelIndex &index) const
{
   if (!index.isValid())
      return QModelIndex();

   CPropItem* childItem = static_cast<CPropItem*>(index.internalPointer());
   CPropItem* parentItem = childItem->getParent();

   if (parentItem ==  m_data.data())
   {
     return QModelIndex();
   }

 //  qDebug() << "CPrimitivesModel::parent  2 rows=" << parentItem->rows() << "parentItem" << parentItem;

   return createIndex(parentItem->row(), 0, parentItem);
}

