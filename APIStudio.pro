QT += quick
CONFIG += c++17

# Add CommonAPI includes
INCLUDEPATH += $$PWD/../toolchain/CommonAPI/capicxx-core-runtime-master/include

# Add CommonAPI/SomeIP Proxy
INCLUDEPATH += ./GenProxy2/
# Add glm
INCLUDEPATH += $$PWD/../toolchain/glm/

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        ARSceneControl.cpp \
        Acc.cpp \
        FasCluster.cpp \
        JSItem.cpp \
        JSModules.cpp \
        LDW.cpp \
        PrimitivesModel.cpp \
        ProjectsList.cpp \
        PropItem.cpp \
        Speedometer.cpp \
        TraceViewModel.cpp \
        augmentedsceneadapter.cpp \
        augmentedscenearadapter.cpp \
        augmentedscenearsignals.cpp \
        augmentedscenesignals.cpp \
        main.cpp \
        warpingcontroladapter.cpp \
        warpingcontrolsignals.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Add libraries
unix:!macx|win32:           LIBS += -L$$PWD/../toolchain/CommonAPI/build/x64-Debug/ -lCommonAPI
win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../toolchain/CommonAPI/build/x64-Debug/CommonAPI.lib

# Default rules for deployment.
target.path: INSTALLS += target

HEADERS += \
    ARSceneControl.h \
    Acc.h \
    AugmentedSceneCommon.hpp \
    ComAPIDefs.h \
    FasCluster.h \
    JSItem.hpp \
    JSModules.h \
    LDW.h \
    PrimtivesModel.h \
    ProjectsList.h \
    PropItem.h \
    Speedometer.h \
    TraceViewModel.h \
    augmentedsceneadapter.h \
    augmentedscenearadapter.h \
    augmentedscenearsignals.h \
    augmentedscenesignals.h \
    tproxyentry.h \
    treginterfaces.h \
    warpingcontroladapter.h \
    warpingcontrolsignals.h

DISTFILES +=
