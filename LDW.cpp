#include "LDW.h"
#include "augmentedscenesignals.h"
#include "QDebug"

CLDW::CLDW(const std::string& name, std::shared_ptr<AugmentedSceneProxy<>> objRef)
    : CARSceneControl(name, objRef)
{

}

void CLDW::initialize(const std::vector<AugmentedSceneTypes::PrimitiveInfo>& primitives_info)
{
   CARSceneControl::initialize(primitives_info);

    AugmentedSceneTypes::Variant valPolyline3(AugmentedSceneTypes::EVariantType::Polyline3, std::vector<v0::Ipc::MathTypes::Vector3>(
    {{0., 1.8, 0.}, {50., 1.5, 0.}}));
   setPropertyValue("left_line.geometry", valPolyline3);

   AugmentedSceneTypes::Variant valText(AugmentedSceneTypes::EVariantType::Text, std::string("Normal"));
   setPropertyValue("left_line.state", valText);
   
    AugmentedSceneTypes::Variant valBool(AugmentedSceneTypes::EVariantType::Bool,false);
    setPropertyValue("left_line.triangles", valBool);
   
    valBool.setValue(true);
    setPropertyValue("visibility.left_line", valBool);

    valPolyline3.setValue( std::vector<v0::Ipc::MathTypes::Vector3>( {{0., -1.8, 0.}, {50., -1.5, 0.}}));
   setPropertyValue("right_line.geometry", valPolyline3);

    valText.setValue(std::string("Normal"));
   setPropertyValue("right_line.state", valText);

   valBool.setValue(false);
   setPropertyValue("right_line.triangles", valBool);

   valBool.setValue(true);
   setPropertyValue("visibility.right_line", valBool);

   sendProperties();
}

void CLDW::updateLDW(bool left_state, bool right_state)
{
    std::string strRightState = right_state ? "Warning" : "Normal";
    std::string strLeftState   = left_state ?    "Warning" :  "Normal";
   AugmentedSceneTypes::Variant valText(AugmentedSceneTypes::EVariantType::Text, strRightState);
   setPropertyValue("right_line.state", valText);

    valText.setValue(strLeftState);
    setPropertyValue("left_line.state", valText);

    sendProperties();
}

double extractValue(const QString& str, const QString& name)
{
    double retVal = 0;
    int indx = str.indexOf(name);
    if (indx <0)
        return retVal;

   int len = str.indexOf(",", indx);
   if (len < 0) len = str.length() - indx;

    QString res = str.mid(indx+name.length(), len-indx-name.length());
    retVal = res.toDouble();

    return retVal;
}

void CLDW::setRightGeometry(QString objLeft, QString objRight)
{
    //qDebug() << "setRightGeometry: objRef" << objRef;
    double valX1 = extractValue(objLeft, "x:");
    double valY1 = extractValue(objLeft, "y:");
    double valZ1 = extractValue(objLeft, "z:");

    double valX2 = extractValue(objRight, "x:");
    double valY2 = extractValue(objRight, "y:");
    double valZ2 = extractValue(objRight, "z:");

    AugmentedSceneTypes::Variant valPolyline3(AugmentedSceneTypes::EVariantType::Polyline3, std::vector<v0::Ipc::MathTypes::Vector3>(
    {{valX1, valY1,valZ1}, {valX2, valY2,valZ2}}));
   setPropertyValue("right_line.geometry", valPolyline3);
}

