#ifndef CLDW_H
#define CLDW_H

#include "ARSceneControl.h"

class CLDW : public  CARSceneControl
{
     Q_OBJECT

public:
    CLDW(const std::string& name, std::shared_ptr<AugmentedSceneProxy<>> objRef);

    void initialize(const std::vector<AugmentedSceneTypes::PrimitiveInfo>& primitives_info);

     Q_INVOKABLE void updateLDW(bool left_state, bool right_state);

    /*experiments */
    Q_INVOKABLE void setRightGeometry( QString objLeft, QString objRight);

protected:
    CLDW() : CLDW("Invalid", nullptr) {}
};

#endif // CLDW_H
