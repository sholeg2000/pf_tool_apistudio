#include "PropItem.h"
#include <QDebug>

CPropItem::CPropItem(const QString& name, EPropType type, CPropItem* parentItem)
 : m_item_name(name), m_parent(parentItem), m_item_type(type)
{

}

CPropItem::~CPropItem()
{
  //  qDebug() << "~CJSItem";
    qDeleteAll(m_subItems);
}
