#ifndef CPrimitivesModel_H
#define CPrimitivesModel_H

#include <QAbstractItemModel>
#include <QSharedPointer>
#include "ComAPIDefs.h"
#include "PropItem.h"


class CPrimitivesModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    CPrimitivesModel(QObject *parent = nullptr);

    Q_INVOKABLE void clearModel();

    void addPrimitives(const std::vector<AugmentedSceneTypes::PrimitiveInfo>& primitives_info);

    // members of QAbstractItemModel
public:
   QVariant data(const QModelIndex &index, int role) const override;
   int rowCount(const QModelIndex &parent = QModelIndex()) const override;
   int columnCount(const QModelIndex &parent = QModelIndex()) const override;
   QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
   QModelIndex parent(const QModelIndex &index) const override;

protected:
   QHash<int, QByteArray> roleNames() const override;

   void addPropItem(CPropItem* parentItem, EPropType type, const QString& text);

signals:
     void outputValue(const QString& primitive,const  QString& attr, const QString& value);

public slots:
      void setValue(const QString& primitive,const  QString& attr, const QString& value); // [OS] Experimental

protected: // Attributes
   enum JSRoles {
       PrimitiveName = Qt::UserRole + 1,
       PropValue,
   };
   QSharedPointer<CPropItem> m_data;  /* Root of the TreeView Model */
};

#endif // CPrimitivesModel_H
