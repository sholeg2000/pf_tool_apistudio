#ifndef AUGMENTEDSCENEADAPTER_H
#define AUGMENTEDSCENEADAPTER_H

#include "tproxyentry.h"
#include "augmentedscenesignals.h"
#include "ComAPIDefs.h"

class CAugmentedSceneAdapter : public TProxyEntry<AugmentedSceneProxy<>>
{
    public:
       CAugmentedSceneAdapter(std::string domain, std::string instance);

       virtual void bindQmlContext(QQmlContext* ctx) override;

       virtual void regJSInterface(QSharedPointer<QJSEngine>& jsEngine) override;

    protected:
      CAugmentedSceneAdapter();

   protected:
      QSharedPointer<CAugmentedSceneSignals> m_AugmentedSceneSignals;
};

#endif // AUGMENTEDSCENEADAPTER_H
