#ifndef CARSCENECONTROL_H
#define CARSCENECONTROL_H

#include <vector>
#include <QSharedPointer>
#include "ComAPIDefs.h"
#include "QObject"

class CARSceneControl : public QObject
{
    Q_OBJECT
public:
    CARSceneControl(QObject *parent = nullptr);
    CARSceneControl(const std::string& name, std::shared_ptr<AugmentedSceneProxy<>> objRef);
    virtual ~CARSceneControl() override;

    void initialize(const std::vector<AugmentedSceneTypes::PrimitiveInfo>& primitives_info);

    void removeObject();
signals:
     void valueChanged(const QString& primitive,const  QString& attr, const QString& value);
protected:

    AugmentedSceneTypes::PrimitiveId getPrimitiveID() const { return m_idPrimitive; }
    AugmentedSceneTypes::ObjectId getObjectID() const { return m_idObject; }

    void setPropertyValue(const std::string& propName, const AugmentedSceneTypes::Variant& value);

    AugmentedSceneTypes::Variant getPropertyValue(const std::string& propName);
    AugmentedSceneTypes::Properties getProperties() { return m_propValues; }

    void sendProperties();

protected:
     std::shared_ptr<AugmentedSceneProxy<>>  m_proxy;

     std::string m_name; /* Primitive Name */
     AugmentedSceneTypes::PrimitiveId m_idPrimitive = 0;
     AugmentedSceneTypes::ObjectId m_idObject = 0;
     std::vector<AugmentedSceneTypes::PropertyInfo> m_propInfo;   // PropertyId, std::string, EVariantType
     AugmentedSceneTypes::Properties   m_propValues;   //  id + value
};

#endif // CARSCENECONTROL_H
