#ifndef CFASCLUSTER_H
#define CFASCLUSTER_H

#include "ARSceneControl.h"

class CFasCluster  : public CARSceneControl
{
   Q_OBJECT
public:
    CFasCluster();
    CFasCluster(const std::string& name, std::shared_ptr<AugmentedSceneProxy<>> objRef);
    ~CFasCluster() override {}


    void initialize(const std::vector<AugmentedSceneTypes::PrimitiveInfo>& primitives_info);

    //  ACC
    Q_PROPERTY(bool enableACC READ isACCEnabled WRITE enableACC)
    Q_PROPERTY(bool setACC_ISO_Img READ isAccISO WRITE setAccISO)
    Q_PROPERTY(bool enableSpeedLimit READ isSpeedLimitEnabled WRITE enableSpeedLimit)

    Q_INVOKABLE void updateFasClusterACC(bool acc_state);
    Q_INVOKABLE void updateFasClusterLDW(bool ldw_state);

protected:
    // ---- ACC Adaptive Cruise Control ----
    bool isACCEnabled() const { return m_Acc_Enabled; }
    void enableACC(bool val);
    bool isAccISO() const { return m_Acc_ISO; }
    void setAccISO(bool val);

    // ---- Speed Limit ----
    bool isSpeedLimitEnabled() const { return m_Enable_Speedlimit; }
    void enableSpeedLimit(bool val);

    void sendFasClusetrProperties();


protected:
    bool m_Acc_Enabled = true;
    bool m_Acc_ISO = false;
    bool m_Enable_Speedlimit = false;
};

#endif // CFASCLUSTER_H
