#include "TraceViewModel.h"
#include "QDebug"

CTraceViewModel::CTraceViewModel()
{

}

int CTraceViewModel::rowCount(const QModelIndex & parent) const
{
   Q_UNUSED(parent)
   return m_data.size();
}

QHash<int, QByteArray> CTraceViewModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[PrimitiveRole] = "Primitive";
    roles[AttrNameRole] = "Attribute";
    roles[ValueRole] = "Value";
    return roles;
}

QVariant CTraceViewModel::data(const QModelIndex & index, int role) const
{
    if (index.row() < 0  || index.row() >= m_data.size() )
        return QVariant();

    auto item = m_data[index.row()];

    QString ret = "Empty";
    switch(role)
    {
        case PrimitiveRole :  ret = item.primitive; break;
        case AttrNameRole : ret = item.attribute; break;
        case ValueRole : ret = item.value; break;
    }

   return QVariant(ret);
}

void CTraceViewModel::clearTrace()
{
    beginResetModel();
      m_data.clear();
    endResetModel();
}

void CTraceViewModel::addValue(const QString& primitive,const  QString& attr, const QString& value)
{
   CTraceItem item;
   item.primitive = primitive;
   item.attribute = attr;
   item.value = value;

   beginInsertRows(QModelIndex(), rowCount(), rowCount());
   m_data.append(item);
   endInsertRows();
}
