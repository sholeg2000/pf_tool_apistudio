#ifndef AUGMENTEDSCENEARSIGNALS_H
#define AUGMENTEDSCENEARSIGNALS_H

#include <QObject>
#include "ComAPIDefs.h"
#include <QSharedPointer>

#include <QQmlEngine>

#include "LDW.h"
#include "Acc.h"

class CPrimitivesModel;

struct CCameraPosition : public QObject
{
    Q_OBJECT

   public slots:
      void setX(double x) { m_poX = x;  }
      void setY(double y) { m_posY = y; }
      void setZ(double z) { m_posZ = z; }

   protected:
      double m_poX = 0.0;
      double m_posY = 0.0;
      double m_posZ = 0.0;
};

struct CCameraRotation :public QObject
{
   Q_OBJECT

   public slots:
      void setParam1(double val)  { param1 = val; }
      void setParam2(double val)  { param2 = val; }
      void setParam3(double val)  { param3 = val; }
      void setPara4(double val)   { param4 = val; }
    protected:
      double  param1 = 0.9999619;  // TODO: Need proper names !!!!
      double  param2 = 0.0;
      double  param3 = 0.0087265;
      double  param4 = 0.0;
};

class CAugmentedSceneARSignals : public QObject
{
   Q_OBJECT

   public:
       CAugmentedSceneARSignals(QObject *parent = nullptr);
       CAugmentedSceneARSignals(std::shared_ptr<AugmentedSceneProxy<>> objRef);
       virtual ~CAugmentedSceneARSignals();

       bool setupARSZoneScene(CPrimitivesModel* model);

       void registerQMLInterface(QQmlContext* ctx);
       void registerJSInterface(QSharedPointer<QJSEngine>& jsEngine);

     // make initialization when Proxy becomes available
     Q_INVOKABLE void checkConnection(CPrimitivesModel* model);
     Q_INVOKABLE void updateCamera(int degreeY);

   signals:

   public slots:
        bool isAvailable() const { return m_connected; }
       void updateCamera(const v0::Ipc::MathTypes::Vector3& position, const v0::Ipc::MathTypes::Quaternion& rotation);

   public:
       CCameraPosition& getCameraPosition() { return m_camera_pos; }
       CCameraRotation& getCameraRotation() { return m_camera_rotation; }

   protected:
       void clearSceneObjects();

   protected:
       std::shared_ptr<AugmentedSceneProxy<>>  m_proxy;

       CLDW  m_ctrlLDW;
       CAcc   m_ctrlACC;

       CCameraPosition m_camera_pos;
       CCameraRotation m_camera_rotation;
       bool m_connected = false;
};

#endif // AUGMENTEDSCENEARSIGNALS_H
