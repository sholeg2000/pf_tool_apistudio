#ifndef AUGMENTEDSCENEARADAPTER_H
#define AUGMENTEDSCENEARADAPTER_H

#include "tproxyentry.h"
#include "ComAPIDefs.h"
#include "augmentedscenearsignals.h"


class CAugmentedSceneARAdapter : public TProxyEntry<AugmentedSceneProxy<>>
{
   public:
    CAugmentedSceneARAdapter(std::string domain, std::string instance);

    virtual void bindQmlContext(QQmlContext* ctx) override;

    virtual void regJSInterface(QSharedPointer<QJSEngine>& jsEngine) override;

   protected:
      CAugmentedSceneARAdapter();

   protected:
      QSharedPointer<CAugmentedSceneARSignals> m_AugmentedSceneARSignals;
};

#endif // AUGMENTEDSCENEARADAPTER_H
