/*
* Implementation of the resent projects list.
* Functionality
*   - Load/Save list of the projects, last used project
*   - Add / remove project
*   - Load/Save projects attributes
*
* Provide ListModel for displaying in QML ProjectsView
*/
#ifndef CPROJECTSLIST_H
#define CPROJECTSLIST_H

#include <QAbstractListModel>
#include <QObject>

class CProjectData;

class CProjectsList : public QAbstractListModel
{
    Q_OBJECT
public:
    CProjectsList(const QString& path);
    ~CProjectsList();


    /* current open project */
    Q_PROPERTY(int currProjID READ getCurrPrjIndx WRITE setCurrPrjIndx NOTIFY projectChanged)

    /* return false if failed to add new project. Project is created in the same directory where executable file is located */
    Q_INVOKABLE bool addProject(const QString& name);

    /* return false if project is not exist for indx, otherwise return true and data */
    Q_INVOKABLE CProjectData* getActiveProjectData() const;
    Q_INVOKABLE bool setActiveProjectData(CProjectData* data);
    /* save all data to persistent storage */
    Q_INVOKABLE void save();

    /* load data from persistence storage. i.e. INI file */
    void initialize();

signals:
    /* Notify that new project was selected / loaded */
    void projectChanged();
    void projectUpdated();

public slots:

 /* QAbstractListModel*/
public:
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
protected:
    QHash<int, QByteArray> roleNames() const;

    enum PRLRoles {
        PrjName = Qt::UserRole + 1,
    };

protected:
    QSharedPointer<CProjectData> getProjectDataByIndex(int indx) const;

    int getCurrPrjIndx() const { return m_currPrjIndx; }
    void setCurrPrjIndx(int indx);


    QVector<QSharedPointer<CProjectData>> m_prjList;
    int m_currPrjIndx = -1;
    QString m_appPath = ""; // path to the application location
};

class CProjectData : public QObject {
    Q_OBJECT
public:
   ~CProjectData();

    CProjectData(const QString& path, const QString& name) : m_path(path), m_name(name) {}
    CProjectData& operator =(const CProjectData& objRef);
    CProjectData(const CProjectData& objRef);

    Q_PROPERTY(QString lowMatrix READ getLowMatrix WRITE setLowMatrix NOTIFY notifyPathChanged)
    Q_PROPERTY(QString midMatrix READ getMidMatrix WRITE setMidMatrix NOTIFY notifyPathChanged )
    Q_PROPERTY(QString highMatrix READ getHighMatrix WRITE setHighMatrix NOTIFY notifyPathChanged)
    Q_PROPERTY(QString prjName READ getProjectName NOTIFY notifyProjectNameChanged)
    Q_PROPERTY(bool enableWarpingCtrl READ getWarpingCtrlEnable WRITE setWarpingCtrlEnable)

    Q_INVOKABLE void refresh() { emit notifyPathChanged(); }

    void setProjectName(const QString& val) { m_name = val; emit notifyProjectNameChanged(); }
    QString getProjectName() const { return m_name; }

    QString getPath() const { return m_path; }

    QString getLowMatrix() const { return warpingLow; }
    void setLowMatrix(const QString& val)  {  warpingLow = val; emit notifyPathChanged(); }

    QString getMidMatrix() const { return warpingMid; }
    void setMidMatrix(const QString& val)  {  warpingMid = val; emit notifyPathChanged(); }

    QString getHighMatrix() const { return warpingHigh; }
    void setHighMatrix(const QString& val)  {  warpingHigh = val; emit notifyPathChanged(); }

    /* return true if matrixes are specified */
    bool isMatrixesDefined() const {
      return ((warpingLow.isEmpty() || warpingLow.contains("<")) || (warpingMid.isEmpty() || warpingMid.contains("<"))
              || (warpingHigh.isEmpty() || warpingHigh.contains("<"))) ? false : true;
    }

    /* true - if WarpingControl service is enabled.
     * Note: It taked efefct after aplication restart */
    bool getWarpingCtrlEnable() const { return warpingCtrl; }

    void setWarpingCtrlEnable(bool state) {  warpingCtrl = state; }
signals:
    void notifyPathChanged();
    void notifyProjectNameChanged();
protected:
    CProjectData() {}

protected:
    QString m_name; /* Project Name */
    QString m_path; /* Project location */
    /* WarpingControl*/
    QString warpingLow = "<...>";  /* Low  Matrix*/
    QString warpingMid = "<...>";  /* Mid  Matrix*/
    QString warpingHigh = "<...>"; /* High Matrix*/
    bool warpingCtrl = false;
};

#endif // CPROJECTSLIST_H
