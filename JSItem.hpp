#ifndef JSITEM_H
#define JSITEM_H

#include <QObject>
#include <QString>
#include <QVector>

#include <QJSEngine>
class CJSModules;

enum EModuleType
{
   EMODULE = 0,
   EJSHANDLER,
   EMETHOD,
   EATTRIBUTE
};

class CJSItem : public QObject
{
    Q_OBJECT
public:
    CJSItem(const QString& name, EModuleType type, QString& dscText, CJSItem* parentItem = nullptr);
    ~CJSItem();

    Q_PROPERTY(QString jsBody READ getJScript WRITE setJScript NOTIFY notifyJSBodyChanged)
    Q_PROPERTY(QString jsValidationText READ getJSValidationRes NOTIFY notifyJSValidated )
    Q_PROPERTY(QString itemDescription READ getDescription)

    Q_INVOKABLE bool isJSHandler() const { return (m_itemType == EJSHANDLER) ? true : false; }
    Q_INVOKABLE void execJS(); /* cyclic run of JScript */
    Q_INVOKABLE void valudateJS();


    CJSItem(const CJSItem& onjRef) {}
    CJSItem& operator =(const CJSItem& objRef) { return *this; }

    QString getName() const { return module_name; }
    QString getDescription() const { return  m_itemDescription; }

    CJSItem* getParent() {  return m_parent; }

    int getChildsCount() const { return m_subItems.size(); }
    CJSItem* child(int row) {  return m_subItems.at(row); }

    int rows() {  return  (m_parent) ? m_parent->m_subItems.indexOf(this) : 0; }
    void appendChild(CJSItem* child) { m_subItems.append(child); }

    void save();
    void load(const QString& path);

    /* open or create JS file */
    void intialize(const QString& path);

    QString getJSValidationRes() const { return m_jsValidationRes; }

    /* model contains interface to get JSEngine */
    void setTreeModel(CJSModules* model) { m_TreeModel = model; }
    QSharedPointer<QJSEngine> getJSEngine();
signals:
    void notifyJSBodyChanged();
    void notifyJSValidated();

protected:
    CJSItem() {}

    void setJScript(const QString& body) { m_jsBody = body; }

    QString getJScript() const { return m_jsBody; }

    bool isFileExists(const QString& path);

protected:
    EModuleType m_itemType = EMODULE;
    QString module_name;
    QString m_path;    /* path to JScript location */
    CJSItem* m_parent = nullptr; /* Attention: QSharedPointer can not be used due to recursive references */
    QVector<CJSItem*> m_subItems;
    QString m_jsBody;  /* JScript */
    QString m_itemDescription; /* Context help. Item description */
    QString m_jsValidationRes;

    CJSModules* m_TreeModel = nullptr; /* used to take JSEngine */
};
#endif // JSITEM_H 
