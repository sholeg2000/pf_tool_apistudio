#pragma once

#include <QDebug>

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <vector>

#include "ComAPIDefs.h"

inline AugmentedSceneTypes::PrimitiveId getPrimitiveId(
    const std::vector<AugmentedSceneTypes::PrimitiveInfo>& primitives_info,
    const std::string& pname,
    std::vector<AugmentedSceneTypes::PropertyInfo>& properties_info)
{
    auto pinfo_it = std::find_if(
        primitives_info.begin(),
        primitives_info.end(),
        [&pname](const AugmentedSceneTypes::PrimitiveInfo& pinfo) {
            return pinfo.getName() == pname;
        });
    if (pinfo_it == primitives_info.end())
    {
        qCritical() << "Couldn't find '" << QString(pname.c_str()) << "' primitive information.";
        return AugmentedSceneTypes::PrimitiveId();
    }

    properties_info = pinfo_it->getProperties();
    return pinfo_it->getId();
}

inline AugmentedSceneTypes::PropertyId getPropertyId(
    const std::vector<AugmentedSceneTypes::PropertyInfo>& properties_info,
    const std::string& pname)
{
    auto pinfo_it = std::find_if(
        properties_info.begin(),
        properties_info.end(),
        [&pname](const AugmentedSceneTypes::PropertyInfo& pinfo) {
            return pinfo.getName() == pname;
        });
    if (pinfo_it == properties_info.end())
    {
        qCritical()  << "Couldn't find '" << QString(pname.c_str()) << "' property information";
        return AugmentedSceneTypes::PropertyId();
    }
    return pinfo_it->getId();
}

/* debug function to print PrimitiveInfo into console */
inline void printPrimitiveInfo(const std::vector<AugmentedSceneTypes::PrimitiveInfo>& primitives_info)
{
  for (const auto& item : primitives_info)
  {
     qDebug() << "Primitive:" << QString(item.getName().data());
     // print Actions
     for (const auto& action : item.getActions())
     {
         qDebug() << "Action:" << QString(action.getName().data());
     }
     // Commands
     for (const auto& cmd : item.getCommands())
     {
        qDebug() << "Command:" << QString(cmd.getName().data());
        // cmd.getParameters()
     }
     // Properties
     for (const auto& prop : item.getProperties() )
     {
        qDebug() << "Property:" <<  QString(prop.getName().data()) << "Type:" << prop.getType();
     }
  }
}

