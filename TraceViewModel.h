#ifndef CTRACEVIEWMODEL_H
#define CTRACEVIEWMODEL_H

#include <QAbstractListModel>

struct CTraceItem;

class CTraceViewModel  : public QAbstractListModel
{
    Q_OBJECT

public:
    CTraceViewModel();

    /* Override */
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;


    Q_INVOKABLE void clearTrace();

public slots:
      void addValue(const QString& primitive,const  QString& attr, const QString& value);

protected:
    QHash<int, QByteArray> roleNames() const;

    enum TraceRoles {
        PrimitiveRole = Qt::UserRole + 1,
        AttrNameRole,
        ValueRole
    };

   QVector<CTraceItem> m_data;
};

struct CTraceItem {
    QString primitive;
    QString attribute;
    QString value;
};

#endif // CTRACEVIEWMODEL_H
